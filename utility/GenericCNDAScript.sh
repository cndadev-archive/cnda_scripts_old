#!/bin/bash
#set -x # For debugging purposes

# Unaltered by you, this script breaks. The script is essentially a skeleton
#  of many of the tools and methods often needed in CNDA scripts
#
# Features needed:
# 1.  Add language to find and parse .xnatPass file 

SCRIPTNAME=GenericCNDAScript
CURDIR=`pwd`
LOGSUBDIR=logs
WRKDIR=$CURDIR

# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} does something so cool."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh (OPTIONS) [-H host -U username -P password] -i inputDataFile -c configFile -o outputDataFile -w workingDir -p project -s subject -e experiment -t pattern -v verbose"	
	echo "   REQUIRED"
	echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
	echo "-U || --userid          User id to make connection."
        echo "                        User id and host are optional if .xnatPass file is used."
	echo ""
	echo "   OPTIONAL"  
        echo "-P || --password        Password can be provided on the command line, in the"
        echo "                        .xnatPass file or when prompted by the system."    
	echo "-i || --input           Path and filename from which to import data"
	echo "-c || --config          Path and filename from which to import configuration information."
	echo "-o || --output          Path and filename to which to export data."
        echo "-w || --workdir         Working directory for execution of this project.  This is the"
        echo "                        to which intermediate or temporary work products and logs are saved."
        echo "-p || --project         Specified CNDA project (or comma-separated list of projects)."
	echo "-s || --subject         Specified CNDA subject (or comma-separated list of subjects)."
	echo "-e || --expt            Specified CNDA experiment (or comma-separated list of expts)."
        echo "-t || --pattern         Pattern to match or filter by (usually a short string)"
        echo "-v || --verbose         Run script in verbose mode."
        echo ""
}

#skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
                        host=$1
			;;
		-U | --userid )
			shift
			userid=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-i | --input )
			shift
			inputfile=$1
			;;
		-c | --config )
			shift
			configfile=$1
			;;
		-o | --output )
			shift
			outputfile=$1
			;;
		-w | --workdir )
			shift
			workdir=$1
			;;
                -p | --project )
                        shift
			project=$1
			;;
		-s | --subject )
                        shift
			subject=$1
			;;
                -e | --expt )
                        shift
			expt=$1
			;;
                -t | --pattern )
                        shift
			pattern=$1
			;;
                -v | --verbose )
			verbose=1
			;;
		-h | --help )
			usage
			exit 1
    esac
    shift
done


# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
	echo "Host required."
	echo ""	
	usage
	exit 1
fi

# Subject is required. If not, echo error message and usage output.
if [ -z "$username" ]; then
	echo "User id required."
	echo ""	
	usage
	exit 1
fi


# If input file supplied, make sure it exists.
if [ ! -z "$input" ]
then
	if [ ! -s "$inputfile" ]; then
            echo "The input file you specified either does not exist or is empty."
            echo ""
            usage
            exit 1
	fi
fi

# If config file supplied, make sure it exists.
if [ ! -z "$config" ]; then
	if [ ! -s "$configfile" ]; then
            echo "The config file you specified either does not exist or is empty."
            echo ""
            usage
            exit 1
        else 
            $configfile
	fi
fi


# If wrkdir file supplied, make sure it exists.
if [ -d "$wrkdir" ]; then
    WRKDIR=$wrkdir
else 
    echo ${wrkdir} does not exist, using $WRKDIR instead
fi


# If verbose, then indicate that (can copy and paste this snippet throughout code)
if [ $verbose ]; then
        echo "Running in verbose mode"
fi


# Get session id
JSESSION=`curl -k -u $userid ""${host}/REST/JSESSION""` 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo "${host} log in failed. Please check your password and retry."
  exit 1
fi
if [ $verbose ]; then
  echo "You were logged in to ${host} successfully"
fi  

# Get the top row from a file (for spreadsheet headers)
#HEADERS=`cat "${input_file}" | head -1`
#if [ $verbose ]; then
#      echo "Your file headers are:  $HEADERS"
#fi


# Get the column number for a specific header (count begins at 1)
#COLNUM=`./getColumnForLabel.sh -s $HEADERS -l <header_name>` 

# Example of storing REST request directly into memory and manipulating data 
# using short-circuit ops
#experimentREST=$(XNATRestClient ${constring}-m GET -remote "$restPath") \
#&&
#echo $experimentREST | grep -q "Status page" \
#&&		
#echo "XNATRestClient call failed: $restPath" \
#|| 
#headers=`echo "$experimentREST" | head -1` \
#&&
#subjIdCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectdata/id` 

# Sort (numerically) by a csv by a given column #
# sort -t , -k ${CATIDCOL},${CATIDCOL}n ${WRKDIR}/.scanInfo

# Get data from a column of csv row in memory
#data=$(echo $<csv_row_data> | cut -d',' -f"<column number>")

# Example of building a REST query to get certain columns
#restPath="/REST/projects/<a project>/experiments?format=csv&xsiType=<xnat:mrSessionData>&columns=xnat:subjectData/id,xnat:subjectAssessorData/id,xnat:subjectAssessorData/label"

# Example to get a specific experiment id (assumes we know experiment label and that id is in column 3)
#=`curl -k -b JSESSIONID=$JSESSION "${host}/REST/projects/$6/experiments?format=csv" | grep $exp | sed -e 's/,/ /g;s/"//g' | awk '{print $3}'`;

# Example of REST query to get file counts
#curl -k -b JSESSIONID=$JSESSION "${host}/REST/projects/<a project>/subjects/<a project>/experiments/<a project>/resources?all=true&format=csv&file_stats=true" > csvresults.txt

# Upload a file via REST
#curl -k -b JSESSIONID=$JSESSION -T <path and filename to be uploaded> -X POST ""${host}/REST/projects/<a project>/subjects/<a subject>/experiments/<an experiment>/resources/<directory under resources>/files/<filename in CNDA>.zip?inbody=true""

# REST download example
#curl -k -b JSESSIONID=$JSESSION ""${host}/REST/projects/<a project>/subjects/<a subject>/experiments/<an experiment>/resources/<directory under resources>/files?format=csv"" > ${outputfile}

# REST example to download files by pattern
#curl -k -b JSESSIONID=$JSESSION "${host}/REST/experiments/<an expt>/DIR/RESOURCES/${pattern}/*?recursive=true&format=zip" > ${WRKDIR}/${exp}_${pattern}.zip;

# REST get stored search results as csv file -- this kind of works, you get the filtered results, but columns
#  are all wrong
#curl -k -b JSESSIONID=$JSESSION "${host}/data/search/saved/<search id>/results?format=csv" > storedSearch.txt

# Zip a directory
#zip -r <path and file>.zip ${<dir to zip>}


# Get the line count for an input file -- if applicable     
# IN_LINECOUNT=`wc -l $inputfile | awk '{print $1}'`
# CFG_LINECOUNT=`wc -l $inputfile | awk '{print $1}'` 
#if [ $verbose ]; then
#      echo "Total line count:  $IN_LINECOUNT"
#fi
                                                          
                                                           
# Loop to iterate through input file
# initialize counter (if header on file, then switch to i=1
let i=0

while [ "$i" -lt "${<line count var>}" ]
do      
let i=$i+1
# Get variable from column 1 of the ith row in a csv file
#variable=`awk 'NR=='$i'' "$<csv file>"  | sed -e 's/,/ /g;s/"//g' | awk '{print $1}'`
#echo variable: $variable

      
# generic logic
if []; then
elif []; then
else
fi
done

# Loop to iterate through project list (same for multiple subjects or experiments)
for curProj in $project
do 
if []; then
elif []; then
else
fi
done

cd $CURDIR
