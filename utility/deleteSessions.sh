#!/bin/bash

#Call to this file should include the location of a csv file with list of experiments to be deleted
#The columns must be in the order Project Subject Experiment with NO HEADER

#script is called using:  filename.sh filename.csv
#filename.csv is set to variable INPUT_FILE
#all output from script sent to a file named in OUTPUT var


INPUT_FILE=$1
USER=jclineAdmin
mydate=$(date +%Y_%m_%d_%M_%S)


####FOR ME: you are using your CNDA account -- enter that password!!!!



#jsession=`curl -k -u $USER "https://cnda.wustl.edu/data/JSESSION"`
jsession=`curl -k -u $USER "https://cnda-dev-jeanet.nrg.mir/data/JSESSION"`


#TESTING
#echo "Value of jsession variable: "
#echo ${jsession} 

echo "Running Experiment Deletion"

OUTPUT="OutputDeletionFile_${mydate}.log"
exec >${OUTPUT} 2>&1

while read line
do
   project=`echo ${line} | awk -F',' '{print $1}'`
   subject=`echo ${line} | awk -F',' '{print $2}'`
   experiment=`echo ${line} | awk -F',' '{print $3}'`
   #error=`curl -k -b JSESSIONID=${jsession} -X DELETE "https://cnda.wustl.edu/data/archive/projects/${project}/subjects/${subject}/experiments/${experiment}"`
   error=`curl -k -b JSESSIONID=${jsession} -X DELETE "https://cnda-dev-jeanet.nrg.mir/data/archive/projects/${project}/subjects/${subject}/experiments/${experiment}"`
     if [ ${error} ]; then
       echo "${project}/${subject}/${experiment} WAS NOT deleted properly"
   else
       echo "${project}/${subject}/${experiment} WAS deleted properly" 
   fi 
done < ${INPUT_FILE} 
