#!/bin/bash


# 	$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#
#	Script to take in a spreadsheet of subjects and experiments to be shared into another project
#   Jeanette Cline  October 2014
#
#   Required files: getColumnForLabel.sh
#	$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


MAIN=/home/NRG/jcline01/Sharing
mydate=$(date +%Y_%m_%d_%M_%S)
UTIL_SCRIPT="${MAIN}/getColumnForLabel.sh"   # Location of external file
OUTPUT="${MAIN}/log/OutputOfSharing_${mydate}.log" # Log file
skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

#Create log dir if it does not exist
if [ ! -d "${MAIN}/log" ]; then
  mkdir "${MAIN}/log"
fi


# Usage output for explaining how to use the program
function usage
{
	echo "XNATBulkShare shares a set of subjects or experiments under a list of projects with a specified project."
	echo ""
	echo "Usage: XNATBulkShare.sh -H host -U username -P password -i inputDataFile "	
	echo "   REQUIRED"
	echo "-i || --input           Path and filename from which to import data."
	echo ""
	echo "-H || --host            Host to connect to."
	echo "-U || --username        Username to make connection. Use username for logging into host (not network username)."
	echo ""
	echo "   OPTIONAL	 "	
	echo "-v || --verbose         Output project, subject and experiment id's during "
	echo "                        processing."
    echo "-P || --password        Password to make connection."
	echo ""
}


# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments, assign to variables
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
            host=$1
			;;
		-U | --username )
			shift
			username=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-i | --input )
			shift
			inputfile=$1
			;;
		-v | --verbose )
			verbose=1
			;;
		-h | --help | * )
			usage
			exit 1
    esac
    shift
done




# Better to use jsession to reduce number of seperate logins when running script
JSESSION=`curl -k -u ${username} ""${host}/REST/JSESSION""`

# Input file is required. If not, echo error message and usage output.
if [ -z "$inputfile" ]; then
	echo "The input file you specified either does not exist or is empty."
	echo ""	
	usage
	exit 1
fi



# Send all output and any errors from this point on to Output file
# If running in verbose mode, additional log data is also sent to the Output file
exec >${OUTPUT} 2>&1


if [ $verbose ]; then
        echo "Running in verbose mode"
		echo "Input file is "$inputfile
fi


# Get first line from input file
dataHeaders=`cat "${inputfile}" | head -1` 

# Find number of lines in input file (minus header row)
let tmpfileCount=`cat ${inputfile} | wc -l`
 
# If verbose, then 
if [ $verbose ]; then
		echo "Length of Input file (minus header) is "$tmpfileCount
fi
 
let max=$tmpfileCount

#Collect values from row and assign to variables
let k=2
while [ "$k" -le ${max} ]
do   
	# pull values from the row in the input file and set to currLine
	currLine=`awk 'NR=='$k'' "${inputfile}"` 
	
		if [ $verbose ]; then
			echo "_____________________________________________________"
			echo "Row number: " $k
			echo "currentLine" $currLine
			echo "util script: " $UTIL_SCRIPT
			echo "headers: " $dataHeaders

		fi
	
	projectColNum=`"${UTIL_SCRIPT}"  -s $dataHeaders -l "Project"`
	projectCurrent=`echo ${currLine} | awk -F',' -v awkvar1=$projectColNum '{print $awkvar1}'` 
			
	
	subjectColNum=`${UTIL_SCRIPT} -s $dataHeaders -l "Subject"`
	subject=`echo ${currLine} | awk -F',' -v awkvar2=$subjectColNum '{print $awkvar2}'`
	subjectLabel=$subject
			
	
	experimentColNum=`${UTIL_SCRIPT} -l "Experiment" -s ${dataHeaders}`  
	experiment=`echo ${currLine} | awk -F',' -v awkvar3=$experimentColNum '{print $awkvar3}'`
	experimentLabel=$experiment
			
	
	projShColNum=`${UTIL_SCRIPT} -l "Project_Share" -s ${dataHeaders}`
	projectShare=`echo ${currLine} | awk -F',' -v awkvar4=$projShColNum '{print $awkvar4}'`

		
	sLabelColNum=`${UTIL_SCRIPT} -l "Subject_Share_Name" -s ${dataHeaders}`
	sLabel=`echo ${currLine} | awk -F',' -v awkvar5=$sLabelColNum '{print $awkvar5}'` 	
		if [ -n $sLabel -a "$sLabel" != "" ]; then
			subjectLabel=$sLabel
		fi

		
	eLabelColNum=`${UTIL_SCRIPT} -l "Experiment_Share_Name" -s ${dataHeaders}`
	eLabel=`echo ${currLine} | awk -F',' -v awkvar6=$eLabelColNum '{print $awkvar6}'` 
		if [ -n $eLabel -a "$eLabel" != "" ]; then
			experimentLabel=$eLabel
		fi

		
	subjectYes=false
	subjIncColNum=`${UTIL_SCRIPT} -l "Share_Subject" -s ${dataHeaders}`
	subjectInclude=`echo ${currLine} | awk -F',' -v awkvar7=$subjIncColNum '{print $awkvar7}'`
		if [[ ( "$subjectInclude" == *"y"* ) ]]; then
			subjectYes=true
		fi

		
	# Send variable values to Output file if running in verbose mode	
	if [ $verbose ]; then

			echo "CurrentProject" $projectCurrent
			echo "subject" $subject
			echo "subject label" $subjectLabel
			echo "experiment" $experiment
			echo "experiment label" $experimentLabel
			echo "ShareProject" $projectShare
			echo "Subject include from spreadsheet" $subjectInclude
			echo "ShareSubject" $subjectYes
			echo "Jsession" ${JSESSION}
			echo "_____________________________________________________"
	fi	
		
		
	
	# Continue if subject OR experiment fields are not null
	if [ -n $subject -o -n $experiment ]; then
	
		# Share subject if there is a value in the field AND share subject field is true
		if [ "$subjectYes" == "true" ]; then
			if [ -n $subject -a "$subject" != " "  ]; then
			# Share the subject
			subjectSharing=`curl -X PUT -k -b JSESSIONID=$JSESSION "${host}/REST/projects/${projectCurrent}/subjects/${subject}/projects/${projectShare}?label=${subjectLabel}"`
			
				if [ $verbose ]; then
					echo "Calling: -remote /REST/projects/${projectCurrent}/subjects/${subject}/projects/${projectShare}?label=${subjectLabel}" #$subjectSharing
					echo "time: " $(date)
					echo "ID for subject: "$subjectSharing
				fi
            fi   		
		fi

		# Share experiment if there is a value in the field
		if [ -n $experiment -a "$experiment" != " " ]; then
			# Share the experiment
			experimentSharing=`curl -X PUT -k -b JSESSIONID=$JSESSION "${host}/REST/projects/${projectCurrent}/subjects/${subject}/experiments/${experiment}/projects/${projectShare}?label=${experimentLabel}"`
			
			if [ $verbose ]; then
					echo "Calling: -remote /REST/projects/${projectCurrent}/subjects/${subject}/experiments/${experiment}/projects/${projectShare}?label=${experimentLabel}"
					echo "time: " $(date)
					echo "ID for experiment: "$experimentSharing
			fi
		fi
	
	else
		echo "Neither an experiment nor subject was specified." 
		echo ""
		usage
		exit 1
	fi
	
	let k=$k+1	
done

# close jsession
curl -X DELETE -k -b JSESSIONID=$JSESSION  "${host}/REST/JSESSION"