#!/bin/sh

function usage
{
	echo "getColumnForLabel takes in a column label and a csv file" 
	echo "and returns the column number of the string." 
	echo "Column count begins with 1."
	echo "If the string is not found, then the response is -1."
	echo ""
	echo "Usage: getColumnForLabel.sh (OPTIONS) [-l label string to match] [-s comma-separated string | -f csv file path ]"	
	echo "   REQUIRED"
	echo "-l || --label   Header label (string) for which you wish to know the column number"
	echo "-f || --file    Comma-separated file with column headers in the first row"
        echo "-s || --string  Comma-separated string"
        echo ""
}

while [ $# -gt 0 ]
do
  if [ $1 == "-f" ]; then
    shift
    FILE=$1
#    echo FILE: $1
    shift 
  elif [ $1 == "--file" ]; then
    shift
    FILE=$1
#    echo FILE: $1
    shift 
  elif [ $1 == "-l" ]; then
    shift
    MATCHSTRING=$1
#    echo MATCHSTRING: $1
    shift
  elif [ $1 == "--label" ]; then
    shift
    MATCHSTRING=$1
#    echo MATCHSTRING: $1
    shift
  elif [ $1 == "-s" ]; then
    shift
    HEADERS=$1
#    echo HEADERS: $1
    shift
  elif [ $1 == "-string" ]; then
    shift
    HEADERS=$1
#    echo HEADERS: $1
    shift
  fi 
done

if [ ! -z "$HEADERS" ]; then
    HEADERS=`echo "$HEADERS" | sed -e 's/"//g'`
elif [ ! -z "$FILE" ]; then
    HEADERS=`awk 'NR=='1'' "$FILE" | sed -e 's/"//g'`
else
    echo ""
    echo No column source provided.
    echo ""
    usage
    exit 0
fi

if [ -z "$MATCHSTRING" ]; then
    echo ""
    echo No match label provided.
    echo ""
    usage
    exit 0
fi


# Set temporary value for string
string="1v20aa"

let i=0
while [ "$string" != "" ]
do
  let i=$i+1;
  string=`echo "$HEADERS" | cut -d "," -f $i`
#  echo $string;
  if [ ! -z "$string" ]; then
    if [ "$string" == "$MATCHSTRING" ]; then
      echo "$i"
      string=""
    fi
  else
     echo "-1"
  fi
done

