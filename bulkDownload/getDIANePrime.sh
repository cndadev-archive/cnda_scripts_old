#!/bin/bash
#set -x # For debugging purposes

# Unaltered by you, this script breaks. The script is essentially a skeleton
#  of many of the tools and methods often needed in CNDA scripts
#
# Features needed:
# 1.  Add language to find and parse .xnatPass file 

SCRIPTNAME=getDIANePrime
CURDIR=`pwd`
LOGSUBDIR=logs
WRKDIR=$CURDIR

# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} does something so cool."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh (OPTIONS) [-H host -U userid -P password] -i inputDataFile -c configFile -o outputDataFile -w workingDir -p project -s subject -e experiment -t pattern -v verbose"	
	echo "   REQUIRED"
	echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
	echo "-U || --userid          User id to make connection."
        echo "                        User id and host are optional if .xnatPass file is used."
	echo ""
	echo "   OPTIONAL"  
        echo "-P || --password        Password can be provided on the command line, in the"
        echo "                        .xnatPass file or when prompted by the system."    
	echo "-i || --input           Path and filename from which to import data"
	echo "-c || --config          Path and filename from which to import configuration information."
	echo "-o || --output          Path and filename to which to export data."
        echo "-w || --workdir         Working directory for execution of this project.  This is the"
        echo "                        to which intermediate or temporary work products and logs are saved."
        echo "-p || --project         Specified CNDA project (or comma-separated list of projects)."
	echo "-s || --subject         Specified CNDA subject (or comma-separated list of subjects)."
	echo "-e || --expt            Specified CNDA experiment (or comma-separated list of expts)."
        echo "-t || --pattern         Pattern to match or filter by (usually a short string)"
        echo "-v || --verbose         Run script in verbose mode."
        echo ""
}

# If an error occurs, this method outputs the error and ends the script
die(){
  echo >&2 "$@"
  exit 1
}

#skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
                        host=$1
			;;
		-U | --userid )
			shift
			userid=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-i | --input )
			shift
			inputfile=$1
			;;
		-c | --config )
			shift
			configfile=$1
			;;
		-o | --output )
			shift
			outputfile=$1
			;;
		-w | --workdir )
			shift
			workdir=$1
			;;
                -p | --project )
                        shift
			project=$1
			;;
		-s | --subject )
                        shift
			subject=$1
			;;
                -e | --expt )
                        shift
			expt=$1
			;;
                -t | --pattern )
                        shift
			pattern=$1
			;;
                -v | --verbose )
			verbose=1
			;;
		-h | --help )
			usage
			exit 1
    esac
    shift
done


# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
	echo "Host required."
	echo ""	
	usage
	exit 1
fi

# Subject is required. If not, echo error message and usage output.
if [ -z "$userid" ]; then
	echo "User id required."
	echo ""	
	usage
	exit 1
fi


# Convert project csv commas to line breaks 
if [ ! -z ${project} ]; then
       project=`echo $project | sed 's/,/ /g'` 
fi


# If input file supplied, make sure it exists.
#if [ ! -z "$input" ]
#then
#	if [ ! -s "$inputfile" ]; then
#            echo "The input file you specified either does not exist or is empty."
#            echo ""
#            usage
#            exit 1
#	fi
#fi

# If config file supplied, make sure it exists.
#if [ ! -z "$config" ]; then
#	if [ ! -s "$configfile" ]; then
#            echo "The config file you specified either does not exist or is empty."
#            echo ""
#            usage
#            exit 1
#        else 
#            $configfile
#	fi
#fi


# If wrkdir file supplied, make sure it exists.
if [ -d "$wrkdir" ]; then
    WRKDIR=$wrkdir
else 
    echo ${wrkdir} does not exist, using $WRKDIR instead
fi


# If verbose, then indicate that (can copy and paste this snippet throughout code)
if [ $verbose ]; then
        echo "Running in verbose mode"
fi


# Get session id
JSESSION=`curl -k -u ${userid} ""${host}/REST/JSESSION""` 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo "${host} log in failed. Please check your password and retry."
  exit 1
fi
if [ $verbose ]; then
  echo "You were logged in to ${host} successfully"
fi

# Loop to iterate through project list (same for multiple subjects or experiments)
for curProj in $project
do
  experimentREST=$(curl -k -b JSESSIONID=$JSESSION "${host}/REST/projects/${curProj}/experiments?format=csv")  
  errorPage=`echo $experimentREST | grep -q "Status page"` 
  if [ ${errorPage} != "" ];
  then
    die "Call failed: " ${experimentREST} 
  fi 
  echo ${experimentREST} 
  headers=`echo "$experimentREST" | head -1` 
  idCol=`$CURDIR/getColumnForLabel.sh -s $headers -l ID` 
  labelCol=`$CURDIR/getColumnForLabel.sh -s $headers -l label` 
  if [ $verbose ]; 
  then 
    echo "idCol: " ${idCol} 
    echo "labelCol: " ${labelCol}
  fi
  echo pattern: ${pattern}
  experimentREST=`echo ${experimentREST} | sed -e 's/"//g'`
  for experimentLine in $experimentREST 
  do
            patternMatch=`echo ${experimentLine} | grep ${pattern}`
            if [ $patternMatch ]; 
            then
               if [ $verbose ]
               then
                  echo "Working on line:" ${experimentLine}
               fi
               experimentId=$(echo $experimentLine | cut -d',' -f${idCol}) 
               #echo "echo $experimentLine | cut -d',' -f${idCol}"
               echo $experimentLine | cut -d',' -f${idCol}
               experimentLabel=$(echo $experimentLine | cut -d',' -f${labelCol})  
               subjLabel=$(echo ${experimentLabel} | cut -d'_' -f1)
               echo ${subjLabel}
               visitLabel=$(echo ${experimentLabel} | cut -d'_' -f2)
               dirName=${curProj}_${subjLabel}_${visitLabel}
               if [ ! -d ${dirName} ];
               then
                   mkdir ${dirName}
               fi
               cd ${dirName}           
               if [ $verbose ]; 
               then
                  echo EXP ID=${experimentId}, EXP Label=${experimentLabel}
                  echo "Calling: curl -k -b JSESSIONID=$JSESSION ${host}/data/experiments/${experimentId}/files?format=csv"
               fi
               fileREST=$(curl -k -b JSESSIONID=$JSESSION "${host}/data/experiments/${experimentId}/files?format=csv")
               errorPage=`echo ${fileREST} | grep -q "Status page"`
               if [ ${errorPage} ];
               then
                   echo "Call failed: " ${fileREST}
               else 
                 fileHeaders=`echo "$fileREST" | head -1`
                 if [ $verbose ];
                 then 
                    echo File headers: $fileHeaders
                 fi 
                 fileURICol=`$CURDIR/getColumnForLabel.sh -s $fileHeaders -l URI` 
                 fileNameCol=`$CURDIR/getColumnForLabel.sh -s $fileHeaders -l Name` 
                 if [ $verbose ];
                    then
                        echo file URI column: ${fileURICol}
                        echo file name: ${fileNameCol}
                 fi
                 fileREST=$(echo "$fileREST" | tail -n +2 | sed -e 's/"//g')
                 for fileLine in $fileREST 
                 do
                    fileURI=$(echo $fileLine | cut -d',' -f${fileURICol}) 
                    fileName=$(echo $fileLine | cut -d',' -f${fileNameCol}) 
                    if [ $verbose ];
                    then
                        echo "Working on line:" ${fileLine}
                        echo file URI: ${fileURI} 
                        echo file name: ${fileName}
                    fi
                    curl -k -b JSESSIONID=$JSESSION "${host}${fileURI}?format=zip" > ./${fileName}
                    unzip ./${fileName}
                    rm *.zip
                    rm *.edat
                    rm *.edat2
                 done
               fi  # fi check to see if file listing returned error
            cd $WRKDIR
            fi # fi cbat experiment 
  done  # loop through experiments
done # loop through projects
