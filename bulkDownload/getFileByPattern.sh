#!/bin/sh
## getFileByPattern.sh <userid> <password> <input file containing sessions> <output directory> <file download path/pattern> 

SCRIPTNAME=getFileByPattern.sh

echo "user id: $1";
echo "session file: $3";
echo "output dir: $4";
echo "file download path/pattern: $5";

# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} downloads CNDA session files by pattern."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh  <userid> <password> <input file containing sessions> <output directory> <file download path/pattern>"	
	echo "REQUIRED"
	echo "userid          CNDA user id"
    echo "password        CNDA password "
    echo "session file    Path and filename from which to import data"
	echo "                format:  project,subject,session"
	echo "                example: ProjA,subj001,subj001_MR1"
	echo "output dir      Directory where downloads will be saved"
    echo "match pattern   Directory pattern to be matched"
    echo ""
}

testfile=$3

if [ ! -z ${testfile} ]; then 
	if [ ! -s ${testfile} ]; then
		echo "${testfile} does not exist or is empty"
		usage
		exit 0
	fi
else 
    echo "No session file provided."
	usage
	exit 0
fi

lineCount=`wc -l $testfile | awk '{print $1}'`
echo line count: $lineCount

let i=0            
while [ "$i" -lt "$lineCount" ]
do
  let i=$i+1;  
  #get project 
  project=`awk 'NR=='$i'' "$testfile" | awk -F',' '{print $1}'`
  echo project: $project
  #get subject
  subject=`awk 'NR=='$i'' "$testfile"  | awk -F',' '{print $2}'`
  echo subject: $subject
  #get experiment
  exp=`awk 'NR=='$i'' "$testfile"  | awk -F',' '{print $3}'`
  echo experiment: $exp
 
  exp_id=`curl -k -u $1:$2 "https://cnda.wustl.edu/data/archive/projects/$project/subjects/$subject/experiments?format=csv" | grep $exp | sed -e 's/,/ /g;s/"//g' | awk '{print $1}'`;
   echo experiment accession id: $exp_id
   echo "Getting files for session $exp";
   curl -k -u $1:$2 "https://cnda.wustl.edu/data/archive/experiments/$exp_id/DIR$5?recursive=true&format=zip" > $4/${exp}.zip;
   echo Finished downloading file $4/${exp}.zip
done 

