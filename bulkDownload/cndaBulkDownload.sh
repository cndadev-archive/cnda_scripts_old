#!/bin/bash

WRKDIR=`pwd`
CFGFILE=$WRKDIR/.RESTresults.tmp
PROGRAM=cndaBulkDownload.sh
USAGE="echo -e Usage: $PROGRAM -u <user id> -p <project>  
 OPTIONAL:
[-t <xsitype> data type (default xnat:mrSessionData)
 -d <download directory> (default ./<project>_downloads)]"
XNATHOST=https://cnda.wustl.edu
XSITYPES=xnat:mrSessionData

#date=`date "+%Y%m%d"`
#echo date: $date

while [ $# -gt 0 ]
do
  if [ $1 == "-u" ]; then
    shift
    USERID=$1
    echo CNDA user id: $1
    shift 
  elif [ $1 == "-p" ]; then
    shift
    PROJECT=$1
    echo project: $1
    shift
  elif [ $1 == "-t" ]; then
    shift
    XSITYPES=$1
    echo xsitypes: $1
    shift
  elif [ $1 == "-d" ]; then 
    shift
    DOWNLOADDIR=$1
    echo download directory: $1
    shift
fi 
done

if [ -z $USERID ]; then
  echo No user id entered 
  #echo $USAGE 
  $USAGE
  exit 0
fi

if [ -z $PROJECT ]; then
  echo No project entered 
  echo $USAGE
  exit 0
fi

# If the download directory wasn't set, then create default directory 
if [ -z $DOWNLOADDIR ]; then
  DOWNLOADDIR="$PROJECT"_downloads
  # echo set download dir to $DOWNLOADDIR
fi

if [ ! -d $DOWNLOADDIR ]; then
  echo Creating download directory: $DOWNLOADDIR 
  mkdir $DOWNLOADDIR
fi
cd $DOWNLOADDIR 

# get session id
JSESSION=`curl -k -u $USERID ""https://cnda.wustl.edu/REST/JSESSION""` 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo CNDA log in failed. Please check your password and retry.
  exit 0
fi
 
# get list of experiment

rm $CFGFILE
curl -k -b JSESSIONID=$JSESSION ""https://cnda.wustl.edu/REST/projects/$PROJECT/experiments?xsiType=$XSITYPES\&format=csv\&columns=xnat:experimentData/ID,xnat:subjectAssessorData/subject_id,xnat:experimentData/label"" > $CFGFILE

lineCount=`wc -l $CFGFILE | awk '{print $1}'`

# Check for invalid lists 
if [ "$lineCount" -lt "2" ]; then
  echo No results were returned from CNDA for this request.
  exit 0;
fi
           
ISHTML=`cat $CFGFILE | grep -i "<html>"`
if [ ! -z $ISHTML ]; then
  echo Your id is probably not enabled for this project.
  echo Please verify that the project id you entered is correct.
  echo If you need further assistance, please contact the CNDA administrator
  echo at cnda-help@wustl.edu. 
  exit 0
fi

# List of sessions is valid, download sessions 
let i=1
let countNum=$lineCount-1
while [ "$i" -lt "$lineCount" ]
do
  let i=$i+1

  # get experiment ID
  exptID=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $1}'`
  # echo experiment: $exptID
  # get subject ID
  subjID=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $2}'`
  # echo subject: $subjID
  # get experiment label 
  label=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $4}'`
  # echo label: $label

  let lineNum=$i-1
  if [ -s $label.zip ]; then
    echo $label.zip \($lineNum of $countNum\) already exists
  else 
    echo Downloading $label for $PROJECT \($lineNum of $countNum\)
    curl -k -b JSESSIONID=$JSESSION ""https://cnda.wustl.edu/REST/projects/$PROJECT/subjects/$subjID/experiments/$exptID/scans/ALL/files?format=zip > $label.zip
  fi

done

rm $CFGFILE
