#!/bin/sh
## downloadFilesWithFilter.sh <userid> <password> <input file containing sessions> <output directory> <file download path/pattern> <project>

## Example: ./downloadFilesWithFilter.sh <user id> <password> ./testfile.txt ./neo305c_download_new volumes wu_premie


echo "user id: $1";
echo "session file: $3";
echo "output dir: $4";
echo "file download path/pattern: $5";
echo "project: $6";

testfile=$3
downloadDir=$4
pattern=$5

if test -f $testfile
then
  echo "file $testfile exists";
else
  echo "file $testfile does not exist";
  exit 0;
fi
    
if [ ! -d $downloadDir ]; then
  mkdir $downloadDir
  echo created $downloadDir
fi 

lineCount=`wc -l $testfile | awk '{print $1}'`
echo line count: $lineCount

let i=0            
while [ "$i" -lt "$lineCount" ]
do
  let i=$i+1;  
  #get experiment
  exp=`awk 'NR=='$i'' "$testfile"  | awk '{print $1}'`
  if [ -z $exp ]; then
     exit 0
  fi
  echo experiment: $exp
 
  exp_id=`curl -k -u $1:$2 "https://128.252.241.243/REST/projects/$6/experiments?format=csv" | grep $exp | sed -e 's/,/ /g;s/"//g' | awk '{print $3}'`;
   echo experiment accession id: $exp_id
echo "Getting files for session $exp";
   curl -k -u $1:$2 "https://128.252.241.243/REST/experiments/$exp_id/DIR/RESOURCES/${pattern}/*?recursive=true&format=zip" > ${downloadDir}/${exp}_${pattern}.zip;
   echo Finished downloading file $downloadDir/"$exp"_"$pattern".zip
done 

