#!/usr/bin/env python
"""
Upload OIS data

Usage:
    ois-upload.py -h HOST -u USER -p PASSW -f CSVFILE INPUT_DATA_DIR
    ois-upload.py --help | --version

Options:
    -h HOST, --host=HOST            XNAT URL (i.e http://localhost/xnat)
    -u USER, --username=USER        XNAT username
    -p PASSW, --password=PASSW      XNAT password
    -f CSVFILE, --file=CSVFILE      CSV file with all necessary info. Script assumes you know the proper format.
    INPUT_DATA_DIR                  Data files should be stored in INPUT_DATA_DIR/{Date}/
    --help                          Show this help message and exit
    --version                       Show version and exit
"""

__version__ = "1"
__author__ = "Flavin"

import os
import re
import sys
import requests
import warnings
import pandas as pd
from docopt import docopt

def die_if_failed(request, message='Failed'):
    if not request.ok:
        print message
        print 'url: ' + request.url
        print request.text
        request.raise_for_status
        sys.exit(1)

# Read the input arguments
args = docopt(__doc__, version=__version__)

# Create XNAT session
s = requests.Session()
s.verify = False
s.auth = (args['--username'], args['--password'])

# Clean up host
host = args['--host']
m = re.match(r'https?://', host)
if not m:
    host = 'https://' + host
if host[-1] == '/':
    host = host.rstrip('/')

inputdir = os.path.abspath(args['INPUT_DATA_DIR'])

# This will store the indices of rows we had to skip. We will print that list at the end.
skipped = []

required_columns = [
    'project',
    'subject_label',
    'session_label',
    'date',
    'system',
    'contrast',
    'framerate',
    'modality'
]

# Pre-compile this useful regex
dateRegex = re.compile(r'(?P<y>\d\d)(?P<m>\d\d)(?P<d>\d\d)')

# Requests prints a lot of ssh warnings, so surround everything with this
#  warning catch block
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    # Try to connect, quit if we failed
    print "Connecting to {}".format(host)
    r = s.post(host + '/data/JSESSION')
    die_if_failed(r, "Connecting to {} failed.".format(host))
    print

    # Read OIS csv file
    oisdata = pd.read_csv(os.path.expanduser(args['--file'])).astype('string').fillna('')
    nrows, ncols = oisdata.shape

    # Iterate through rows. Create sessions, upload files, start pipelines
    for rowidx in xrange(nrows):
        # Make sure row has values for all the required columns
        blanks = filter(lambda col: pd.isnull(oisdata[col][rowidx]) or oisdata[col][rowidx] == '' or oisdata[col][rowidx] == 'nan', required_columns)
        if blanks:
            if len(blanks) > 2:
                message = "values for columns " + ", ".join(blanks[:-1]) + ", and " + blanks[-1]
            elif len(blanks) == 2:
                message = "values for columns " + " and ".join(blanks)
            else:
                message = "the value for column " + blanks[0]
            print "Row {} is missing {}".format(rowidx, message)
            print "Skipping\n"
            skipped.append(rowidx)
            continue

        date = oisdata['date'][rowidx]
        datedir = os.path.join(inputdir, date)
        if not os.access(datedir, os.R_OK):
            print "Cannot read date directory {}.\nSkipping.\n".format(datdir)
            skipped.append(rowidx)
            continue

        os.chdir(datedir)

        proj = oisdata['project'][rowidx]
        subj = oisdata['subject_label'][rowidx]
        sess = oisdata['session_label'][rowidx]
        print "Beginning upload for subject {}, session {}".format(subj, sess)

        # Create subject
        subjurl = host + '/data/projects/{}/subjects/{}'.format(proj, subj)
        r = s.get(subjurl)
        if not r.ok:
            params = {
                'xsiType': 'xnat:subjectData'
            }
            for field in ['cohort', 'strain', 'species']:
                if oisdata[field][rowidx]:
                    params['xnat:subjectData/fields/field[name={}]/field'.format(field)] = oisdata[field][rowidx]
                    print "{} = {}".format(field, oisdata[field][rowidx])

            m = dateRegex.match(oisdata['dob'][rowidx])
            if m:
                yyyymmdd = '-'.join(('20'+m.group('y'), m.group('m'), m.group('d')))
                params['dob'] = yyyymmdd
                print "dob = {}".format(yyyymmdd)

            print "Creating subject {}".format(subj)
            del r
            r = s.put(subjurl, params=params)
            die_if_failed(r, "Creating subject {} failed.".format(subj))
            print "Done\n"
        else:
            print "Subject {} already exists\n".format(subj)

        # Create session
        sessurl = host + '/data/projects/{}/subjects/{}/experiments/{}'.format(proj, subj, sess)
        r = s.get(sessurl)
        if not r.ok:
            params = {
                'xsiType': 'opti:oisSessionData',
                'scanner': oisdata['system'][rowidx],
                'opti:oisSessionData/contrast': oisdata['contrast'][rowidx],
                'opti:oisSessionData/framerate': oisdata['framerate'][rowidx]
            }
            m = dateRegex.match(oisdata['date'][rowidx])
            if m:
                yyyymmdd = '-'.join(('20'+m.group('y'), m.group('m'), m.group('d')))
                params['date'] = yyyymmdd

            print "Creating session {}".format(sess)
            del r
            r = s.put(sessurl, params=params)
            die_if_failed(r, "Creating session {} failed.".format(sess))
            print "Done\n"
        else:
            print "Session {} already exists\n".format(sess)

        # Upload data for each scan file
        scanFileRegex = re.compile('{}-{}-([_a-zA-Z]+)([0-9]+).tif'.format(oisdata['date'][rowidx], oisdata['subject_label'][rowidx]))
        print "Uploading files to session {}".format(sess)
        for file in os.listdir(datedir):
            if not '{}-{}'.format(oisdata['date'][rowidx], oisdata['subject_label'][rowidx]) in file:
                # This is a file for some other session
                continue

            m = scanFileRegex.match(file)
            if m:
                # This is a scan file
                scantype, scanid = m.groups()
                scanurl = host + "/data/projects/{}/subjects/{}/experiments/{}/scans/{}".format(proj, subj, sess, scanid)
                r = s.get(scanurl)
                if not r.ok:
                    print "Creating scan {}".format(scanid)
                    del r
                    r = s.put(scanurl, params={"xsiType": "opti:oisScanData", "type": scantype})
                    die_if_failed(r, "Creating scan {} failed.".format(scanid))
                else:
                    print "Scan {} already exists".format(scanid)

                scanresourceurl = host + "/data/projects/{}/subjects/{}/experiments/{}/scans/{}/resources/TIF".format(proj, subj, sess, scanid)
                r = s.get(scanresourceurl)
                if not r.ok:
                    print "Uploading file {} to scan {}".format(file, scanid)
                    del r
                    r = s.put(scanresourceurl, params={"format": "TIF", "content": "RAW"})
                    die_if_failed(r, "Creating scan {} failed.".format(scanid))
                    r = s.put(scanresourceurl+"/files", files={'file': open(file, 'rb')})
                    die_if_failed(r, "Creating scan {} failed.".format(scanid))
                else:
                    print "Scan {} already contains file {}".format(scanid, file)
            else:
                # This is a resource file
                resource = "REGISTER" if "LandmarksandMask.mat" in file else "AUXILIARY"
                resourcefileurl = host + "/data/projects/{}/subjects/{}/experiments/{}/resources/{}".format(proj, subj, sess, resource)
                r = s.get(resourcefileurl)
                if not r.ok:
                    print "Uploading resource file {}".format(file)
                    r = s.put(resourcefileurl)
                    die_if_failed(r, "Uploading resource file {} failed.".format(file))
                    r = s.put(resourcefileurl+"/files", files={'file': open(file, 'rb')})
                    die_if_failed(r, "Uploading resource file {} failed.".format(file))
                else:
                    print "Session {} already has file {}".format(sess, file)

        print "Done uploading files\n"

        # Launch pipeline
        params = {param: oisdata[param][rowidx] for param in
            ['rs_lowpass', 'rs_highpass', 'rs_samplingrate', 'stim_lowpass', 'stim_highpass', 'stim_samplingrate', 'stim_blocksize', 'stim_baseline', 'stim_duration']
            if pd.notnull(oisdata[param][rowidx]) and oisdata[param][rowidx] != '' and oisdata[param][rowidx] != 'nan'}
        pipelineurl = host + '/data/projects/{}/pipelines/ois/experiments/{}'.format(proj, sess)
        print "Launching OIS pipeline for session {}".format(sess)
        print "Params: {}".format(params)
        r = s.post(pipelineurl, params=params)
        die_if_failed(r, "Could not launch OIS pipeline for session {}".format(sess))
        print "Launched\n"

print "All done\n"

if skipped:
    skippedData = oisdata.iloc[skipped, :]

    skipped = map(str, skipped)
    if len(skipped) > 2:
        message = "I skipped rows " + ", ".join(skipped[:-1]) + ", and " + skipped[-1]
    elif len(skipped) == 2:
        message = "I skipped rows " + " and ".join(skipped)
    else:
        message = "I skipped row " + skippedRows[0]

    print message

    skippedCsv = args['--file']+".skipped"
    print "Writing skipped data to " + skippedCsv
    try:
        skippedData.to_csv(os.path.expanduser(skippedCsv))
        print "Add the missing data to {}, then run with that as the input csv.".format(skippedCsv)
    except:
        print "Failed. You'll have to get the skipped rows out of {} yourself.".format(args['--file'])
