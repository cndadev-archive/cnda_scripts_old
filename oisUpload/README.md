# ois-upload.py
Upload OIS data to an XNAT

    Usage:
        ois-upload.py -h HOST -u USER -p PASSW -f CSVFILE INPUT_DATA_DIR
        ois-upload.py --help | --version

    Options:
        -h HOST, --host=HOST            XNAT URL (i.e http://localhost/xnat)
        -u USER, --username=USER        XNAT username
        -p PASSW, --password=PASSW      XNAT password
        -f CSVFILE, --file=CSVFILE      CSV file with all necessary info. Script assumes you know the proper format.
        INPUT_DATA_DIR                  Data files should be stored in INPUT_DATA_DIR/{Date}/
        --help                          Show this help message and exit
        --version                       Show version and exit

Script reads each line of the CSV file and creates a new session. It will look through `INPUT_DATA_DIR/{Date}/` for files that begin with the session label and upload each to the proper place within the session. When all files for a session have been uploaded, script launches the OIS pipeline for the session, passing it any parameters that are in the optional columns (see CSV format below); if no parameters are found, pipeline is launched with all defaults.

# CSV file format
Each row represents a session. Column headers are as follows.

## Required columns
Script will skip a row if it finds a blank in one of these columns, or if it cannot read the `INPUT_DATA_DIR/{date}` directory.

* project
* date
* subject_label
* session_label (by convention, equals `${date}-${subject_label}`)
* modality
* system (value restricted to `fcOIS1` or `fcOIS2` for now)
* contrast (value restricted to `hemoglobin` for now)
* framerate

## Optional columns
* operator
* species
* cohort
* strain
* dob
* rs_lowpass
* rs_highpass
* rs_samplingrate
* stim_lowpass
* stim_highpass
* stim_samplingrate
* stim_blocksize
* stim_baseline
* stim_duration

# Requirements

Python (script was tested on 2.7)

## Non-standard-library packages

* [requests](http://docs.python-requests.org/en/latest/)
* [pandas](http://pandas.pydata.org)
* [docopt](http://docopt.org/)