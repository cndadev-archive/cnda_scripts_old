#!/bin/bash
# James Ransford - 01/06/12
# Script locates files and directories with 
# spaces in the name withen the specified archive path

ARCHIVE=/data/nil-bluearc/marcus/CNDA
LOGS=./space_log

if [ ! -d $LOGS ]; then
mkdir $LOGS
fi

# Set the delimiter to newline
IFS=$'\n'

while read project; do 
   echo "|> Project: $project"
   if [ ! -e $LOGS/$project.space.log ]; then
      find $ARCHIVE/$project -type f | grep [[:space:]] >> $LOGS/$project.space.log
   else
      echo " $LOGS/$project.space.log exists. Skipping ..."
   fi
done

# Reset the delimiter
unset IFS

# Remove log files with size 0
find $LOGS -type f -size 0 -exec rm {} \;
