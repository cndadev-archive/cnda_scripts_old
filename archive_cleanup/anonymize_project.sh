#!/bin/bash
# James Ransford - 12/12/2011
# Pipe in a list of projects and this script will
# Anonymize all dicom and or ima files in the project.

# Script requires dcmodify and dcmdump from
# DCMTK - http://dicom.offis.de/dcmtk 

# Path to CNDA archive.
ARCHIVE=/data/nil-bluearc/marcus/CNDA

# archive_anon/ directory is created here.
LOG_DIR=`pwd`

# Path to the dcmtk bin directory. 
DCMTK_BIN=$LOG_DIR/archive_anon/dcmtk/bin/

# Create directory for logs if it doesn't exist.
if [ ! -d $LOG_DIR/archive_anon ]; then
   echo "Creating directory $LOG_DIR/archive_anon"
   mkdir $LOG_DIR/archive_anon
fi

# Create the sessions completed file. 
if [ ! -e $LOG_DIR/archive_anon/sessions_completed ]; then
  touch $LOG_DIR/archive_anon/sessions_completed
fi 

# Set the delimiter to \n so we can catch files with
# spaces in the name. 
IFS=$'\n'

# Don't look at files with the following in the name
# This is just to narrow down the number of files we run the file command on.  
# We know that files that have the following in their name will never be DICOM or IMA. 
FILES_WITHOUT="((\/PROCESSED\/)|(\/ASSESSORS\/)|(\.4dfp\.)|(\/SNAPSHOTS\/)|((\.xml|\.gif|\.log|\.err|\.gz|\.rec|\.dat|\.conc|\.lst|\.m3z|\.mgz)$))"

while read project; do 
   echo "" && echo "[INFO] >> Project: $project"
   # Look for the arc directories for this project (ex. arc001, arc002)
   for arc in `ls -1 $ARCHIVE/$project | egrep "arc[0-9]{3}"`; do
      if [ -d $ARCHIVE/$project/$arc ]; then      
         # Check for session directories inside this arc
         for session in $ARCHIVE/$project/$arc/*; do
            if [ -d $session ]; then
               if [ -z "$(cat $LOG_DIR/archive_anon/sessions_completed | egrep "$session")" ]; then 
                  echo "" && echo "|> Checking Session:  $arc/`basename $session`"          
                  for file in `find $session -type f | egrep -v $FILES_WITHOUT`; do
                     # If DICOM, edit with dcmodify
                     if [ ! -z "$(file -b $file | grep 'DICOM medical imaging data')" ]; then 
                        # Did we find a DICOM file?
                        echo "|DICOM|> $file "
                        $DCMTK_BIN/dcmodify -q -nb -ie -e "(0010,0030)" -e "(0008,0050)" -e "(0010,1010)" -e "(7053,1000)" -e "(7053,1001)" -e "(7053,1002)" -e "(7053,1003)" $file
                     # If IMA, edit with xxd
                     elif [ ! -z "$(xxd -s 0x60 -l 7 -c 7 $file | grep 'SIEMENS')" ]; then
                        # Check to see if the birthdate is listed as 01-JAN-1901
                        echo "|IMA|> $file "
                        echo "0001744: 30312d4a414e2d31383030" | xxd -r - $file 
                        echo "0000353: 39393959" | xxd -r - $file
                    fi  
                  done # for file in
                  echo "|SESSION|> $arc/`basename $session` - Anonymization Complete "
                  echo "$session - Anonymization Complete `date`" >> $LOG_DIR/archive_anon/sessions_completed 
               else
                  echo "|SESSION|> $arc/`basename $session` has already been anonymized.  Skipping..".
               fi
            fi
         done # for session in
      fi
   done # for arc in
   echo "[INFO] >> Project: $project --> DONE."
   echo "$project scrubbed on `date`" >> $LOG_DIR/archive_anon/projects_completed
done # while


# Reset the delimiter
unset IFS
