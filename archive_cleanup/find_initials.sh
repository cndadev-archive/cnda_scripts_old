#!/bin/bash
# James Ransford - 1/04/2012
# This script finds sessions and subjects within a project
# that could potentially have subject initals in the label. 

#CNDA url
CNDA_URL=localhost:8080

# CNDA Login credentials. 
CNDA_USERNAME=YourCNDAUserName
CNDA_PASSWORD=YourCNDAPassword

# make sure the person using this has entered the proper credentials.
if [ $CNDA_USERNAME == "YourCNDAUserName" ]; then
   echo "|> ERR:  CNDA Username  not set!"
   echo "|> INFO: Please modify this script to set CNDA_USERNAME" && exit 1
elif [ $CNDA_PASSWORD == "YourCNDAPassword" ]; then
   echo "|> ERR:  CNDA Password not set!"
   echo "|> INFO: Please modify this script to set CNDA_PASSWORD" && exit 1
fi

# Create some directories for logs.
mkdir -p logs/sessions
mkdir logs/subjects

while read project; do 
   echo "" && echo "[INFO] >> Project: $project"
   
   # Make a rest call to retrieve all the subject labels within a project.  Then check those labels against a regular expression. 
   curl -k -u $CNDA_USERNAME:$CNDA_PASSWORD "$CNDA_URL/data/archive/projects/$project/subjects?format=csv" | sed 1d | cut -d, -f3 | tr -d '"' |\
   egrep "^[A-Za-z]{2,3}($|(-|_)+[A-Za-z1-9]*)" >> logs/subjects/$project.log
   
   # Next look at the session labels.  
   curl -k -u $CNDA_USERNAME:$CNDA_PASSWORD "$CNDA_URL/data/archive/projects/$project/experiments?format=csv" | sed 1d | cut -d, -f6 | tr -d '"' |\
   egrep "^[A-Za-z]{2,3}($|(-|_)+[A-Za-z1-9]*)" >> logs/sessions/$project.log

done # while

# Remove log files with size 0
find logs -type f -size 0 -exec rm {} \;
