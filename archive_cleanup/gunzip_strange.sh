#!/bin/bash
# James Ransford - 12-9-11
# Script takes a list of projects as paramaters and 
# gunzips the files listed gzipped_and_strange.log for each project.
#
# Run log_gzipped_files.sh to generate gzipped_and_strange.log(s)

MASTER_LIST_PATH=$HOME
PROJECT_LOGS_PATH=$HOME

# If the project has a gzipped_and_strange.log, append its 
# contents to the master list.
append_project_log_to_master(){
echo -n "|>> Checking Project: $1"
if [ -d $PROJECT_LOGS_PATH/project_logs/$1 ]; then
   if [ -e $PROJECT_LOGS_PATH/project_logs/$1/gzipped_and_strange.log ]; then
     echo "  --> Adding gzipped_and_strange.log to gzipped_and_strange_master.log"
     # Ignore files ending with .nii.gz, .mgz, .m3z, .img.gz, .log.gz, and .tgz
     cat $PROJECT_LOGS_PATH/project_logs/$1/gzipped_and_strange.log | egrep -v '/PROCESSED/|\.nii\.gz$|\.mgz$|\.m3z$|\.img\.gz$|\.log\.gz$|\.tgz$|\.tar\.gz$|\.obj\.gz$|\.lkup\.gz$|\.hdr\.gz$|\.img\.rec\.gz$|\.ifh\.gz$|\.lst\.gz$|\.log\.old\.gz$|\.mat\.gz$|\.dat\.gz$' >> $MASTER_LIST_PATH/gzipped_and_strange_master.log
   else
     echo "  --> gzipped_and_strange.log does not exist. Skipping ..."
   fi
else
  echo " --> Project log does not exist. Skipping ..."
fi
}

# Gunzips each file in gzipped_and_strange_master.log
gunzip_master_log(){
echo "|>> Gunzipping all files listed in $MASTER_LIST_PATH/gzipped_and_strange_master.log"
#for file in `cat $HOME/gzipped_and_strange_master.log`; do 
cat $MASTER_LIST_PATH/gzipped_and_strange_master.log | while read file; do
   if [ -e "$file" ]; then  # Does the file exist?
      if [ ! -z "`file -b "$file" | grep 'gzip compressed data'`" ]; then # Is the file actually gzipped?
         if [ -z "`ls "$file" | egrep '\.gz$'`" ]; then # Does the file have the .gz extension?  
            mv -v "$file" "$file.gz"
            gunzip -v "$file.gz" || echo "[FAILED] $file.gz --`date`" >> failed.log
         else
            gunzip -v "$file" || echo "[FAILED] $file --`date`" >> failed.log
        fi  
      else
         echo "$file is not gzipped.  Skipping..."
      fi
   else
      echo "$file does not exist! - `date`" 
   fi 
done
echo "|>> DONE. "
}

# Build the master log if we need it, otherwise use the existing copy. 
if [ ! -s $MASTER_LIST_PATH/gzipped_and_strange_master.log ]; then
   echo "|>> Could not find $MASTER_LIST_PATH/gzipped_and_strange_master.log"
   echo "    --> Generating new copy... " 
   ls -1 $PROJECT_LOGS_PATH/project_logs/ |  while read project; do append_project_log_to_master $project; done
   echo "|>> Done generating gzipped_and_strange_master.log - Total: `cat $MASTER_LIST_PATH/gzipped_and_strange_master.log | wc -l` gzipped files. "
else
   echo "|>> Found existing $MASTER_LIST_PATH/gzipped_and_strange_master.log"
   echo "    --> Using existing copy."
fi

read -n1 -p "Do you wish to gunzip these files now? (y/n) " < /dev/tty 
echo "" && echo ""

if [ "$REPLY" == "y" -o "$REPLY" == "Y" ]; then
   # If the master log is not empty, gunzip the files listed inside
   if [ -s $MASTER_LIST_PATH/gzipped_and_strange_master.log ]; then
      gunzip_master_log
   else
      echo "|>> Unable to find $MASTER_LIST_PATH/gzipped_and_strange_master.log.  "
      echo "    --> File is either missing or empty."
   fi
fi 
