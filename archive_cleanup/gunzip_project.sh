#!/bin/bash
# James Ransford - 12/1/11 
# Script reads log files created by log_gzipped_files.sh.
#    * files_that_need_gz_ext.log - script appends .gz and gunzips all files listed.
#    * files_with_gz_ext.log - gunzips all files listed.
# Run log_gzipped_files projectName before running this script
# to create log files.  

PROJECT_LOGS_DIR=$HOME/project_logs


# gunzip_files_without_gz_ext()
# Function checks for the log file files_that_need_gz_ext.log which
# contains a listing of all gzipped files that DO NOT have .gz extension.
# For each file listed, the function appends the .gz to the filename and
# gunzips the file. Once complete the file NEED_GZ_DONE is created which
# indicates the operation has been completed. 
gunzip_files_without_gz_ext(){ 
echo "[INFO] >> Checking for files_that_need_gz_ext.log"
if [ -r $HOME/project_logs/$1/files_that_need_gz_ext.log ]; then
   if [ ! -e $HOME/project_logs/$1/NEED_GZ_DONE ]; then
      cat $HOME/project_logs/$1/files_that_need_gz_ext.log | while read file
      do
         # Make sure the file really exists
         if [ -e $file ]; then
            # Make sure that the file is actually gzipped before we do anything 
            # we will regret.
            if [ ! -z "$(file $file | egrep ': gzip compressed data')" ]; then
               echo "   --> Moving $file to $file.gz"
               mv "$file" "$file.gz"
   
               echo "   --> gunzip -v $file.gz"
               gunzip -v "$file.gz"
            else
               echo "[ERR] >> $file is not a gzipped file! Skipping..."
            fi
         else
            echo "[ERR] >> $file doesn't even exist! Skipping..."
         fi
      done
      touch $HOME/project_logs/$1/NEED_GZ_DONE
   else
      echo "   --> files_that_need_gz_ext.log already processed. Skipping..."
   fi
else
   echo "   --> files_that_need_gz_ext.log does not exist for project $1. Skipping..." 
fi
}

# gunzip_files_with_gz_ext()
# Function checks for the log file files_with_gz_ext.log which contains a 
# listing of all gzipped files that DO HAVE the .gz extension.
# The function gunzips each file listed in the log.  Once complete the file
# HAVE_GZ_DONE is created which indicates the operation has been completed. 
gunzip_files_with_gz_ext(){
echo "[INFO] >> Checking for files_with_gz_ext.log"
if [ -r $HOME/project_logs/$1/files_with_gz_ext.log ]; then
   if [ ! -e $HOME/project_logs/$1/HAVE_GZ_DONE ]; then
      cat $HOME/project_logs/$1/files_with_gz_ext.log | while read file
      do
         # Make sure the file really exists
         if [ -e $file ]; then
            # Make sure that the file is actually gzipped before we do anything 
            # we will regret.
            if [ ! -z "$(file $file | egrep ': gzip compressed data')" ]; then
               echo "   --> gunzip -v $file"
               gunzip -v "$file"
            else
               echo "[ERR] >> $file is not a gzipped file. Skipping..."
            fi
         else
            echo "[ERR] >> $file doesn't even exist! Skipping..."
         fi
      done
      touch $HOME/project_logs/$1/HAVE_GZ_DONE
   else
      echo "   --> files_with_gz_ext.log already processed. Skipping..."
   fi 
else
   echo "   --> files_with_gz_ext.log does not exist for project $1. Skipping..."
fi
}

# Check for args
if [ $# -ne 1 ]; then
   echo "Usage: $0 project_name"
   echo ""
   exit
# Make sure project_log/project exists
elif [ ! -d $HOME/project_logs/$1 ]; then
   echo "[ERR] >> ~/project_logs/$1 does not exist!"
   echo "[INFO] >> Unable to read log files."
   echo "[INFO] >> Call './log_gzipped_files $1' to create log files for project $1."
   echo ""
   exit
else
   IFS=$'\n'
   echo "[INFO] >> Processing project: $1"
   gunzip_files_without_gz_ext $1 
   gunzip_files_with_gz_ext $1
   echo "[INFO] >> Project: $1 ... DONE!" && echo ""
   unset IFS
fi


