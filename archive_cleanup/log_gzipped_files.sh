#!/bin/bash                                                                                       
# log_gzipped_files.sh
# James Ransford - 11/23/11
# Script finds and logs the name of all gzipped files within a project. 
# Files are divided into thre logs
#  * Gzipped files with dcm.gz or ima.gz extensions. (files_with_gz_ext.log)
#  * Gzipped files with .dcm or .ima extensions. (files_without_gz_ext.log)
#  * Gzipped files that don't meet the above conditions (gzipped_and_strange.log)
# Log files are stored in $HOME/project_logs/$PROJECT/
# Once log files are created run ./gunzip_project projectName to 
# gunzip all gzipped files listed in the log files.


# Function recursivly searches the project 
# directory and logs all gzipped files. 
process_project(){ 
   echo "   --> Searching directory $1 ..."
   DIRECTORY=$1/*
   # Loop through the contents of the directory
   for entry in $DIRECTORY
   do
      # If entry is a directory, make recursive call
      if [ -d "$entry" ]; then
         process_project $entry
      else
         # Check if the file type is gzip with the file command
         if [ ! -z "$(file $entry | egrep ': gzip compressed data')" ]; then
            if [ ! -z "$(ls $entry | egrep -i '\.dcm\.gz$|\.ima\.gz$')" ]; then
               # File is gzipped and has the extension
               echo "$entry" >> $HOME/project_logs/$PROJECT/files_with_gz_ext.log
            elif [ ! -z "$(ls $entry | egrep -i '\.dcm$|\.ima$')" ]; then
               # File is gzipped but id does not have the extension
               echo "$entry" >> $HOME/project_logs/$PROJECT/files_that_need_gz_ext.log
            else
               # Log all the files that don't get caught above.
               echo "$entry" >> $HOME/project_logs/$PROJECT/gzipped_and_strange.log
            fi 
         fi  
      fi  
   done
}

# Main function. Sets up necessary directories and variables 
# then calls process_project().
main(){
   # Save the project name that we are working on.
   PROJECT=$(basename $1)

   # Create project_logs directory if it does not exist.
   if [ ! -d $HOME/project_logs ]; then
      echo "[INFO] Creating directory $HOME/project_logs"
      mkdir $HOME/project_logs
   fi

   # Create project directory
   if [ ! -d $HOME/project_logs/$PROJECT ]; then
      echo "[INFO] Creating directory $HOME/project_logs/$PROJECT"
      mkdir $HOME/project_logs/$PROJECT
 
      # Change delimiter to \n. (some file/dir names have spaces)
      IFS=$'\n'

      # Recursivly search project directory and log all gzipped files.
      echo "[INFO] Checking project: $PROJECT"
      process_project $1 
      echo "[INFO] Project: $PROJECT --> Done!"

      # Reset delimiter.
      unset IFS
   else
      echo "[INFO] Project: $PROJECT already processed. Skipping ..."
   fi
}

# Check args.
if [ $# -ne 1 ]; then
   # If number of arguments is not 1, print help dialog. 
   echo "Usage: $0 /path/to/files"
   echo "     Script searches through the given directory and logs all" 
   echo "     gzipped files and all gzip files that don't have a the .gz extension. "
   exit
else
   # Execute main function.
   main $1
fi
