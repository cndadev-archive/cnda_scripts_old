#!/bin/sh
## enableUsersByFilter.sh <userid> <password> <input file containing user ids> <file download path/pattern> 

## Example: ./downloadFilesWithFilter.sh <user id> <password> ./testfile.txt ./neo305c_download_new volumes wu_premie


echo "user id: $1";
echo "enable user id file: $3";
echo "file download path/pattern: $4";

testfile=$3
pattern=$4
TEMPCSV=./.temp_projects.csv

if test -f $testfile
then
  echo "file $testfile exists";
else
  echo "file $testfile does not exist";
  exit 0;
fi
    

curl -k -u $1:$2 "https://cndatest.wustl.edu/REST/projects?columns=ID,alias&format=csv" | grep -i ${pattern} | sed -e 's/"//g' | sed -e 's/,/ /g'> $TEMPCSV;
jlineCount=`wc -l $TEMPCSV | awk '{print $1}'`
echo j line count: $jlineCount
let j=0  
last=""
exit 0
while [ "$j" -lt "$jlineCount" ]
do
    let j=$j+1;
    proj=`awk 'NR=='$i'' "$TEMPCSV"  | awk '{print $1}'`
    if [ "$last" -ne "$proj" ]; then
        echo project: $j $proj
        lineCount=`wc -l $testfile | awk '{print $1}'`
        echo line count: $lineCount
        
        let i=0            
        while [ "$i" -lt "$lineCount" ]
        do
              let i=$i+1;  
              #get experiment
              user=`awk 'NR=='$i'' "$testfile"  | awk '{print $1}'`
              if [ -z $user ]; then
                 exit 0
              fi
                       
              curl -k -u $1:$2 "https://cndatest.wustl.edu/REST/projects/${proj}/users/collaborator/${user}"
        done 
        last=$proj
        exit 0
     fi

done

