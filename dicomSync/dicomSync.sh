#!/bin/bash
#set -x # For debugging purposes

# Features to Add
#
# -Comma delimited list of shared projects
# -File containing comma delimited list of shared projects
# -Study/experiment date range
# -Only allow items that have passed QC
#
DCMUTILSBINPATH=/data/nil-bluearc/marcus/CNDA_TOOLS/dicomTools/dicom/bin
export DCMDICTPATH=/data/nil-bluearc/marcus/CNDA_TOOLS/dicomTools/dicom/lib/dicom.dic
REMAP=/data/nil-bluearc/marcus/DicomBrowser-1.5-SNAPSHOT/bin/DicomRemap
PATH=$PATH:$DCMUTILSBINPATH
HOMEDIR=/data/nil-bluearc/marcus/CNDA_TOOLS/dicomCompare/data
CURRDIR=`pwd`
LOGFILE=$HOMEDIR/compareLog.txt
DASDIR=$HOMEDIR/.tmp
CNDAFILEPATH=/data/nil-bluearc/marcus/CNDA
# CNDAFILEPATH=$HOMEDIR/.tmp
NEWDIRSUFFIX=_remapNewDir
ARCHIVEDIRSUFFIX=_archivedByDicomRemap

touch $LOGFILE
# Usage output for explaining how to use the program
function usage
{
	echo "dicomSync compares the subject and session labels to dicom fields and prints out conflicting lines."
	echo ""
	echo "Usage: dicomSync.sh (OPTIONS) [-H host -U username -P password] [-p primary_project(,primary_project...) | -f primary_projectsfile]"	
	echo "   REQUIRED"
	echo "-p || --primaryprojects Comma delimited list of primary projects for the "
	echo "                        subjects or experiments that will be shared with the "
    echo "                        shared project."
	echo "-f || --projectsfile    File containing comma delimited list of primary"
	echo "                        projects with subjects or experiments that will"
	echo "                        be shared with the shared project."
	echo "                        (Either -p or -f are required, not both.)"
	echo ""
	echo "-H || --host            Host to connect to."
	echo "-U || --username        Username to make connection."
	echo "-P || --password        Password to make connection."
	echo "                        Username, password and host are optional if .xnatPass "
	echo "                        file is used. All must be provided to make a connection."
	echo ""
	echo "   OPTIONAL	 "	
	echo "-v || --verbose         Output project, subject and experiment id's during "
	echo "                        processing."
	echo "-x || --xsitype         Select an XSI type."
	echo "-F || --filter          Select a string by which the REST results should be filtered.  Only results which include this string will be used."
	echo ""
}

skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
            host=$1
			;;
		-U | --username )
			shift
			username=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-p | --primaryprojects )
			shift
			primaryprojects=$1
			;;
		-f | --projectsfile )
			shift
			projectsfile=$1
			;;
		-x | --xsitype )
			shift
			xsiType="&xsiType=${1}"
			;;
                -F | --filter )
                        shift
			filterStr=$1
			;;
		-v | --verbose )
			verbose=1
			;;
		--experiment )
			type="experiment"
			;;
		-h | --help | * )
			usage
			exit 1
    esac
    shift
done

# If username, password and host are provided, set the connection string
if [ "$username" -a "$password" -a "$host" ]
then
	constring="-host ${host} -u ${username} -p ${password} "
	skipRows=2
fi


# If projcts to be shared is not provided, Dispaly error message and usage output.
if [ -z "$primaryprojects" ]
then
	if [ -f "$projectsfile" ]
	then
		primaryprojects=`cat ${projectsfile}`
	else
		echo "project list data was not provided"
		echo ""
		usage
		exit 1
	fi
fi

old_IFS=${IFS}
IFS=","

mkdir $HOMEDIR/.tmp
for project in $primaryprojects
do 
	if [ $verbose ] 
	then
	    echo PROJECT=${project}
	fi
	
	IFS=$'\n'

        restPath="/REST/projects/${project}/experiments?format=csv${xsiType}&columns=xnat:subjectData/id,xnat:subjectAssessorData/id,xnat:subjectAssessorData/label"
        if [ $verbose ] 
        then
                echo "Calling: XNATRestClient ${constring}-m GET -remote \"$restPath\""
        fi
        experimentREST=$(XNATRestClient ${constring}-m GET -remote "$restPath") \
        &&
        echo $experimentREST | grep -q "Status page" \
        &&		
        echo "XNATRestClient call failed: $restPath" \
        &&
        echo "Error Message: $experimentREST" \
        &&
        echo "" \
        || 
        headers=`echo "$experimentREST" | head -1` \
        &&
        subjIdCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectdata/id` \
        &&
        expIdCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectassessordata/id` \
        &&
        expLabelCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectassessordata/label` \
        &&
        (experimentREST=$(echo "$experimentREST" | tail -n +$skipRows | sed -e 's/"//g' ) 
        if [ ! -z $filterStr ]; then 
            experimentREST=`echo "$experimentREST" | grep "$filterStr"`
        fi \
        &&
        for experimentLine in $experimentREST 
        do
                experiment=$(echo $experimentLine | cut -d',' -f"$expIdCol") 
                experimentLabel=$(echo $experimentLine | cut -d',' -f"$expLabelCol") 
                subject=$(echo $experimentLine | cut -d',' -f"$subjIdCol") 
                
                if [ $verbose ] 
                then
                        echo SUBJECT=$subject, EXPERIMENT=$experiment
                        echo "Calling: XNATRestClient ${constring}-m PUT -remote \"/REST/projects/${project}/subjects/${subject}/experiments/${experiment}/projects/${sharedproject}?label=${experimentLabel}\""
                fi

                # Get a valid scan id 
                scanREST=`XNATRestClient ${constring}-m GET -remote "/REST/experiments/${experiment}/scans?format=csv" | head -n 2`
                echo "$scanREST"
                scanheaders=`echo "$scanREST" | sed -e 's/"//g' | head -1`
                echo scanheaders: "$scanheaders" 
                scanIDCol=`./getColumnForLabel.sh -s $scanheaders -l ID`
                echo scanIDCol: "$scanIDCol" 
                tailRow=`echo "$scanREST" | tail -1 | sed -e 's/"//g'`
                echo tailRow: "$tailRow"
                scanID=`echo "$tailRow" | cut -d',' -f"$scanIDCol"`    
                echo scanID: $scanID       
                isDicom=`XNATRestClient ${constring}-m GET -remote "/REST/projects/${project}/subjects/${subject}/experiments/${experiment}/scans/${scanID}/resources/DICOM/files" | grep dcm`
                if [ -z "$isDicom" ]; then
                   echo ${project} ${subject} ${experimentLabel}  No scan ${scanID} dicom >> $LOGFILE
                else
                   XNATRestClient ${constring}-m GET -remote "/REST/projects/${project}/subjects/${subject}/experiments/${experiment}/scans/${scanID}/resources/DICOM/files?format=zip" > $HOMEDIR/.tmp/${experimentLabel}.zip
                   if [ ! -s /$HOMEDIR/.tmp/${experimentLabel}.zip ]; then
                      echo ${project} ${subject} ${experimentLabel}  No download >> $LOGFILE
                   else
			# Get the subject label
			subjectREST=`XNATRestClient ${constring}-m GET -remote /REST/projects/${project}/subjects?format=csv&columns=label`
                        echo subjectREST: $subjectREST
			headers=`echo "$subjectREST" | head -1` 
                	labelCol=`./getColumnForLabel.sh -s $headers -l label` 
			subjectLine=`echo "$subjectREST" | grep ${subject} | sed -e 's/"//g'` 
			subjectLabel=`echo $subjectLine | cut -d',' -f"$labelCol"`

		      cd $HOMEDIR/.tmp
                      unzip $HOMEDIR/.tmp/${experimentLabel}.zip
                      cd $HOMEDIR/.tmp/${experimentLabel}
		      dicomProj=`for h in *; do find $h -name '*.dcm' -print | xargs dcmdump --search 8,1030 | sort | uniq | sed -n -e '/\[[^]]/s/^[^[]*\[\([^]]*\)].*$/\1/p'; done`
                      dicomSubj=`for h in *; do find $h -name '*.dcm' -print | xargs dcmdump --search 10,10 | sort | uniq | sed -n -e '/\[[^]]/s/^[^[]*\[\([^]]*\)].*$/\1/p'; done` 
                      dicomExp=`for h in *; do find $h -name '*.dcm' -print | xargs dcmdump --search 10,20 | sort | uniq | sed -n -e '/\[[^]]/s/^[^[]*\[\([^]]*\)].*$/\1/p'; done`
                      dicomFilePath=$CNDAFILEPATH/${project}/arc001/${experimentLabel}
                      # dicomFilePath=$CNDAFILEPATH/${experimentLabel}
                      if [ "$project" = "$dicomProj" ]; then
                         echo ${project} ${subjectLabel} ${experimentLabel} Project OK >> $LOGFILE 
                      else
                         echo ${project} ${subjectLabel} ${experimentLabel} Project mismatch: ${dicomProj} >> $LOGFILE
                         touch ${DASDIR}/${experimentLabel}.das
                         cat >>${DASDIR}/${experimentLabel}.das <<EOF
(0008,1030) := "${project}"
EOF
                      fi
                      if [ "$subjectLabel" = "$dicomSubj" ]; then
                         echo echo ${project} ${subjectLabel} ${experimentLabel} Subject OK >> $LOGFILE 
                      else
                         echo ${project} ${subjectLabel} ${experimentLabel} Subject mismatch: ${dicomSubj} >> $LOGFILE
touch ${DASDIR}/${experimentLabel}.das
                         cat >>${DASDIR}/${experimentLabel}.das <<EOF
(0010,0010) := "${subjectLabel}"
EOF
                      fi
                      if [ "$experimentLabel" = "$dicomExp" ]; then
                	echo ${project} ${subjectLabel} ${experimentLabel} Experiment OK >> $LOGFILE     
			else
                         echo ${project} ${subjectLabel} ${experimentLabel} Experiment mismatch: ${dicomExp} >> $LOGFILE
touch ${DASDIR}/${experimentLabel}.das
                         cat >>${DASDIR}/${experimentLabel}.das <<EOF
(0010,0020) := "${experimentLabel}"
EOF
                      fi
                      if [ -s ${DASDIR}/${experimentLabel}.das ]; then
                        if [ -d ${dicomFilePath} ]; then
                          if [ ! -d ${dicomFilePath}${ARCHIVEDIRSUFFIX} ]; then 
                            ${REMAP} -d ${DASDIR}/${experimentLabel}.das -o ${dicomFilePath}${NEWDIRSUFFIX} ${dicomFilePath}
                            cp -rf ${dicomFilePath} ${dicomFilePath}${ARCHIVEDIRSUFFIX}
                            zip -r ${dicomFilePath}${ARCHIVEDIRSUFFIX}.zip ${dicomFilePath}${ARCHIVEDIRSUFFIX}
                            if [ -s ${dicomFilePath}${ARCHIVEDIRSUFFIX}.zip ]; then
                              cp -rf  ${dicomFilePath}${NEWDIRSUFFIX}/${experimentLabel}/* ${dicomFilePath}
                              rm -rf ${dicomFilePath}${NEWDIRSUFFIX}
                              rm -rf ${dicomFilePath}${ARCHIVEDIRSUFFIX}
                            else 
                              echo ${project} ${subjectLabel} ${experimentLabel}: Copy of new archive directory failed. >> $LOGFILE
                              exit 0
                            fi
                          else
                            echo Not able to remap ${project} ${subjectLabel} ${experimentLabel}: Dicom archive folder ${dicomFilePath}${ARCHIVEDIRSUFFIX} already exists >> $LOGFILE
                          fi
                        else 
                          echo Not able to remap ${project} ${subjectLabel} ${experimentLabel}: Dicom file path ${dicomFilePath} does not exist  >> $LOGFILE
                        fi
                      fi
                    fi
                    rm $HOMEDIR/.tmp/${experimentLabel}.zip
                    rm -rf $HOMEDIR/.tmp/${experimentLabel}
                    # rm ${DASDIR}/${experimentLabel}.das
                    cd $CURRDIR
                 fi
                   
        done)
done
# rm -rf $HOMEDIR/.tmp

IFS=${old_IFS}

