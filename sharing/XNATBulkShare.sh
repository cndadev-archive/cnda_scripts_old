#!/bin/bash
#set -x # For debugging purposes

# Features to Add
#
# -Comma delimited list of shared projects
# -File containing comma delimited list of shared projects
# -Study/experiment date range
# -Only allow items that have passed QC
#

# Usage output for explaining how to use the program
function usage
{
	echo "XNATBulkShare shares a set of subjects or experiments under a list of projects with a specified project."
	echo ""
	echo "Usage: XNATBulkShare.sh (OPTIONS) [-H host -U username -P password] [--subject | --experiment] [-p primary_project(,primary_project...) | -f primary_projectsfile] [-s shared_project]"	
	echo "   REQUIRED"
	echo "-p || --primaryprojects Comma delimited list of primary projects for the "
	echo "                        subjects or experiments that will be shared with the "
    echo "                        shared project."
	echo "-f || --projectsfile    File containing comma delimited list of primary"
	echo "                        projects with subjects or experiments that will"
	echo "                        be shared with the shared project."
	echo "                        (Either -p or -f are required, not both.)"
	echo "-s || --sharedproject   The project subjects/experiments are being shared to."
	echo ""
	echo "      --subject         Share subjects with project."
	echo "      --experiment      Share experiments with project."
	echo "                        (Either --subject or --experiment are required, not"
	echo "                        both.)"
	echo ""
	echo "-H || --host            Host to connect to."
	echo "-U || --username        Username to make connection."
	echo "-P || --password        Password to make connection."
	echo "                        Username, password and host are optional if .xnatPass "
	echo "                        file is used. All must be provided to make a connection."
	echo ""
	echo "   OPTIONAL	 "	
	echo "-v || --verbose         Output project, subject and experiment id's during "
	echo "                        processing."
	echo "-x || --xsitype         Select an XSI type."
	echo "-F || --filter          Select a string by which the REST results should be filtered."
	echo ""
}

skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
            host=$1
			;;
		-U | --username )
			shift
			username=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-s | --sharedproject )
			shift
			sharedproject=$1
			;;
		-p | --primaryprojects )
			shift
			primaryprojects=$1
			;;
		-f | --projectsfile )
			shift
			projectsfile=$1
			;;
		-x | --xsitype )
			shift
			xsiType="&xsiType=${1}"
			;;
                -F | --filter )
                        shift
			filterStr=$1
			;;
		-v | --verbose )
			verbose=1
			;;
		--subject )
			type="subject"
			;;
		--experiment )
			type="experiment"
			;;
		-h | --help | * )
			usage
			exit 1
    esac
    shift
done

# If username, password and host are provided, set the connection string
if [ "$username" -a "$password" -a "$host" ]
then
	constring="-host ${host} -u ${username} -p ${password} "
	skipRows=2
fi

# Shared project is required. If not, echo error message and usage output.
if [ -z "$sharedproject" ]
then
	echo "Shared project ID required."
	echo ""	
	usage
	exit 1
fi

# If projcts to be shared is not provided, Dispaly error message and usage output.
if [ -z "$primaryprojects" ]
then
	if [ -f "$projectsfile" ]
	then
		primaryprojects=`cat ${projectsfile}`
	else
		echo "project list data was not provided"
		echo ""
		usage
		exit 1
	fi
fi

old_IFS=${IFS}
IFS=","

for project in $primaryprojects
do 
	if [ $verbose ] 
	then
		echo PROJECT=${project}
	fi
	
	IFS=$'\n'

	if [ "$type" == "subject" ]
	then
		restPath="/REST/subjects?project=${project}&format=csv"
		if [ $verbose ] 
		then
			echo "Calling: XNATRestClient ${constring}-m GET -remote \"$restPath\""
		fi

		subjectREST=$(XNATRestClient ${constring}-m GET -remote "$restPath") \
		&&
		echo $subjectREST | grep -q "Status page" \
		&&
		echo "XNATRestClient call failed: $restPath" \
		&&
		echo "Error Message: $subjectREST" \
		&&
		echo "" \
		||
                headers=`echo "$subjectREST" | head -1` \
                &&
                idCol=`./getColumnForLabel.sh -s $headers -l ID` \
                &&
                labelCol=`./getColumnForLabel.sh -s $headers -l label` \
                &&
		(subjectREST=$(echo "$subjectREST" | tail -n +$skipRows | sed -e 's/"//g' ) \
	        &&
                for subjectLine in $subjectREST
		do	
			subject=$(echo $subjectLine | cut -d',' -f"$idCol") 
			subjectLabel=$(echo $subjectLine | cut -d',' -f"$labelCol")

			if [ $verbose ] 
			then
				echo SUBJECT=$subject
				echo "Calling: XNATRestClient ${constring}-m PUT -remote \"/REST/projects/${project}/subjects/${subject}/projects/${sharedproject}?label=${subjectLabel}\""
			fi
			# Line to add DIAN_ALL to a subject
			XNATRestClient ${constring}-m PUT -remote "/REST/projects/${project}/subjects/${subject}/projects/${sharedproject}?label=${subjectLabel}"
		done) 

	elif [ "$type" == "experiment" ]
	then
		restPath="/REST/projects/${project}/experiments?format=csv${xsiType}&columns=xnat:subjectData/id,xnat:subjectAssessorData/id,xnat:subjectAssessorData/label"
		if [ $verbose ] 
		then
			echo "Calling: XNATRestClient ${constring}-m GET -remote \"$restPath\""
		fi
		experimentREST=$(XNATRestClient ${constring}-m GET -remote "$restPath") \
		&&
		echo $experimentREST | grep -q "Status page" \
		&&		
		echo "XNATRestClient call failed: $restPath" \
		&&
		echo "Error Message: $experimentREST" \
		&&
		echo "" \
		|| 
                headers=`echo "$experimentREST" | head -1` \
                &&
                subjIdCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectdata/id` \
                &&
                expIdCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectassessordata/id` \
                &&
                expLabelCol=`./getColumnForLabel.sh -s $headers -l xnat:subjectassessordata/label` \
                &&
		(experimentREST=$(echo "$experimentREST" | tail -n +$skipRows | sed -e 's/"//g' ) 
                if [ ! -z $filterStr ]; then 
                    experimentREST=`echo "$experimentREST" | grep "$filterStr"`
                fi \
                &&
		for experimentLine in $experimentREST 
		do
			experiment=$(echo $experimentLine | cut -d',' -f"$expIdCol") 
			experimentLabel=$(echo $experimentLine | cut -d',' -f"$expLabelCol") 
			subject=$(echo $experimentLine | cut -d',' -f"$subjIdCol") 
			
			if [ $verbose ] 
			then
				echo SUBJECT=$subject, EXPERIMENT=$experiment
				echo "Calling: XNATRestClient ${constring}-m PUT -remote \"/REST/projects/${project}/subjects/${subject}/experiments/${experiment}/projects/${sharedproject}?label=${experimentLabel}\""
			fi

			# Line to add DIAN_ALL to a experiment
			XNATRestClient ${constring}-m PUT -remote "/REST/projects/${project}/subjects/${subject}/experiments/${experiment}/projects/${sharedproject}?label=${experimentLabel}"
		done)
	else
		echo "Neither an experiment nor subject was specified." 
		echo ""
		usage
		exit 1
	fi
done

IFS=${old_IFS}

