#!/bin/bash

for h in *_*; do if [ -d "$h" ]; then echo $h; echo -n "$h," >> VoxelsVolumesAggregated.csv; head -1 $h/fslstats_b_000_voxels.txt | tr -d '\n' >> VoxelsVolumesAggregated.csv; echo -n "," >> VoxelsVolumesAggregated.csv; head -1 $h/fslstats_b_000_volume.txt >> VoxelsVolumesAggregated.csv; fi; done
