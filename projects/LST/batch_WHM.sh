#!/bin/sh

DATAPATH=
ROOT_RESULTS=	
LSTPATH=/nrgpackages/tools.release/spm8_r6313/toolbox/LST
SPMPATH=/nrgpackages/tools.release/spm8_r6313
SCRIPTPATH=/scripts
source /nrgpackages/scripts/fsl5_setup.sh

input_csv=$1
	
while read line
do
   t1=""
   flair=""
      
   # Everything looks good, prepare the build directory
   procDir=""
   session=`echo ${line} | cut -d, -f3`
   t1=`echo ${line} | cut -d, -f4`
   flair=`echo ${line} | cut -d, -f5`
   procDir=${ROOT_RESULTS}/${session}
   if [ ! -d ${procDir} ]; then
      mkdir -p ${procDir}/t1
      mkdir -p ${procDir}/flair
      echo procDir $procDir
      logDir=${procDir}/logs
      mkdir ${logDir}
   fi
   t1File=`find ${DATAPATH}/${session}/scans/${t1}* -name '*.nii'`
   if [ -z ${t1File} ]; then
      dcm2niix ${DATAPATH}/${session}/scans/${t1}*
   fi
   flairFile=`find ${DATAPATH}/${session}/scans/${flair}* -name '*.nii'`
   if [ -z ${flairFile} ]; then
      dcm2niix ${DATAPATH}/${session}/scans/${flair}*
   fi
   t1File=`find ${DATAPATH}/${session}/scans/${t1}* -name '*.nii'`
   flairFile=`find ${DATAPATH}/${session}/scans/${flair}* -name '*.nii'`
   t1Name=`echo ${t1File} | rev | cut -d/ -f1 | rev`
   flairName=`echo ${flairFile} | rev | cut -d/ -f1 | rev`
   fslreorient2std ${t1File} ${procDir}/t1/${t1Name}
   fslreorient2std ${flairFile} ${procDir}/flair/${flairName}
   t1File=`find ${procDir}/t1 -name '*.nii'`   
   flairFile=`find ${procDir}/flair -name '*.nii'`

   #Ready to make a job
   jobFile=${procDir}/job.cmd
   touch ${jobFile}
   echo "#!/bin/sh" >> ${jobFile}
   echo "source /nrgpackages/scripts/fsl5_setup.sh" >> ${jobFile}
   echo "export MATLABROOT=/usr/local/MATLAB/R2015a" >> ${jobFile} 
   echo "export MATLAB_JAVA=/usr/lib/jvm/java/jre" >> ${jobFile}
   echo "matlab -nosplash -nodisplay -r "\""addpath('${SPMPATH}'); addpath('${SCRIPTPATH}'); addpath('${LSTPATH}'); LST '${t1File}' '${flairFile}' ; exit"\"" &> ${logDir}/LST.stdout" >> ${jobFile}
   echo "fslstats ${procDir}/t1/b_000* -V | cut -d' ' -f2 > ${procDir}/fslstats_b_000_volume.txt" >> ${jobFile}
   echo "slicer ${procDir}/flair/rm*.nii -l Greyscale ${procDir}/t1/b_000* -l Red -S 4 2000 ${procDir}/${session}_snapshot.png" >> ${jobFile} 
   echo "fslchfiletype NIFTI_GZ ${procDir}/t1/b_000*.nii" >> ${jobFile}
   echo "fslchfiletype NIFTI_GZ ${procDir}/flair/rm*.nii" >> ${jobFile}
   if [ ! -f ${procDir}/fslstats_b_000_volume.txt ]; then
      qsub -V -N LST${runNum1}_${session} -o ${logDir} -e ${logDir} -M gurneyj@mir.wustl.edu -m eas -l mem_free=1.9G ${jobFile}
   fi
done < ${input_csv}
