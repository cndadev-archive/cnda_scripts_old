#!/bin/bash

# Pull data from the archive and generate a report file for each scanner group.
# This could be modified to be more flexible so that the user can provide the header titles.

SCRIPTNAME=Weekly_Archive_Report

# Set default working directory
CURDIR=`pwd`
WRKDIR=$CURDIR

# Location of the getColumnForLabel.sh script
UTILDIR=/data/CNDA/cron/utilities/

function usage
{
   echo ""
   echo "${SCRIPTNAME} generates reports for archived sessions."
   echo ""
   echo "Usage: ${SCRIPTNAME}.sh -H host -U username [-P password] -s scannersfile -S storedsearches -r reportlocation [-w workingDir -v]"   
   echo "   REQUIRED"
   echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
   echo "-U || --userid          User id to make connection to CNDA."
   echo "-s || --scannersfile    File containing the scanner group, scanner list and email recipient list."
   echo "-S || --storedsearches  List of the stored searchs."
   echo "-r || --reportsdir      Directory for storing report files"
   echo ""
   echo "   OPTIONAL"
   echo "-P || --password       Password can be provided on the command line."
   echo "-w || --workdir        Location of the report generation scripts."
   echo "-T || --timeStamp      A timestamp to add to the end of the file name of the report."
   echo "-v || --verbose        Run script in verbose mode."
   echo ""
}

if [ $# == 0 ]
then
   echo ""
      echo "No arguments provided"
      echo ""
      usage
   exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
   case $1 in
      -H | --host )
         shift
         host=$1
         ;;
      -U | --userid )
         shift
         userid=$1
         ;;
      -s | --scannersfile )
         shift
         scannerFile=$1
         ;;
      -S | --storedsearches )
         shift
         storedSearchList=$1
         ;;
      -r | --reportsdir )
         shift
         reportsDir=$1
         ;;
      -P | --password )
         shift
         password=$1
         ;;
      -w | --workdir )
         shift
         workdir=$1
         ;;
      -v | --verbose )
         verbose=1
         ;;
      -T | --timeStamp )
         shift
         timeStamp=$1
         ;;
      -h | --help )
         usage
         exit 1
    esac
    shift
done

# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
   echo "Host required."
   echo ""  
   usage
   exit 1
fi

# Subject is required. If not, echo error message and usage output.
if [ -z "$userid" ]; then
   echo "User id required."
   echo ""  
   usage
   exit 1
fi

# The name of a stored search is required. If not, echo error message and usage output.
if [ -z "$storedSearchList" ]; then
   echo "At least one name of a stored search is required."
   echo ""  
   usage
   exit 1
fi

# Location for generated report files is required. If not, echo error message and usage output.
if [ -z "$reportsDir" ]; then
   echo "Location for generated report files is required."
   echo ""  
   usage
   exit 1
fi

# Scanner file is required. If not, echo error message and usage output.
if [ -z "$scannerFile" ]; then
   echo "Scanner file is required."
   echo ""  
   usage
   exit 1
fi

# Read in from the scanner list file
scannerFileList=`cat ${scannerFile}`

# If wrkdir was supplied, make sure it exists.
if [ -d "$workdir" ]; then
   WRKDIR=$workdir
else 
   echo Work directory \"${workdir}\" does not exist, using \"${WRKDIR}\" instead
fi

# If verbose, then indicate that (can copy and paste this snippet throughout code)
if [ $verbose ]; then
   echo "Running in verbose mode"
fi

# Get session id
if [ -z "$password" ]; then
   JSESSION=`curl -k -u ${userid} ""${host}/REST/JSESSION""`
else  
   JSESSION=`curl -k -u ${userid}:${password} ""${host}/REST/JSESSION""`
fi
responseCount=`echo $JSESSION | wc -m`

if [ $responseCount -gt 33 ]; then
   echo ${host} log "in" failed. Please check your password and retry.
   exit 1
fi  
if [ $verbose ]; then
   echo "You were logged onto ${host} successfully"
fi

#Check if directory for report files exists and create it if it doesn't.
if [ ! -e "$reportsDir" ]; then
   if [ $verbose ]; then
      echo "Directory ${reportsDir} created."
   fi
   mkdir $reportsDir
else
   if [ $verbose ]; then
      echo "Directory ${reportsDir} already exists."
   fi
fi

#Loop through the provided stored searches
IFS=","
for storedSearch in $storedSearchList; do
   unset IFS

   # Pull results from the stored search
   if [ $verbose ]; then
      echo ${storedSearch}: Executing REST request: "curl -k -b JSESSIONID=$JSESSION \"${host}/data/search/saved/${storedSearch}/results?format=csv\" > ${WRKDIR}/.storedSearch"
   fi
   
   curl -k -b JSESSIONID=$JSESSION "${host}/data/search/saved/${storedSearch}/results?format=csv" > ${WRKDIR}/.storedSearch
   
   storedSearchHeader=`cat ${WRKDIR}/.storedSearch | head -1`
   sessionList=`cat ${WRKDIR}/.storedSearch | tail -n+2`

   if [ $verbose ]; then
      echo "${storedSearch}: Header found: $storedSearchHeader"
   fi

   # Check if an error occurs and HTML is returned

   badRequest=`echo ${storedSearchHeader} | grep -i "<html>"`
   if [ ! -z "$badRequest" ]; then
      if [ $verbose ]; then
         echo "${storedSearch}: Data returned from stored search is invalid."
      fi

      continue
   fi

   # Find project column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l project"
   fi
   projectCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l project`
   if [ $verbose ]; then
      echo "${storedSearch}: Project column: $projectCol"
   fi

   # Find project aliases column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l mr_project_aliases"
   fi
   projectAliasesCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l mr_project_aliases`
   if [ $projectAliasesCol -eq -1 ]; then
      if [ $verbose ]; then
         echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l ct_project_aliases"
      fi
      projectAliasesCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l ct_project_aliases`
      if [ $projectAliasesCol -eq -1 ]; then
         if [ $verbose ]; then
            echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l pet_project_aliases"
         fi
         projectAliasesCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l pet_project_aliases`
         if [ $projectAliasesCol -eq -1 ]; then
            if [ $verbose ]; then
               echo "${storedSearch}: Unable to find project aliases header"
            fi
         fi
      fi
   fi
   if [ $verbose ]; then
      echo "${storedSearch}: Project aliases column: $projectAliasesCol"
   fi

   # Find session label column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l label"
   fi
   labelCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l label`
   if [ $verbose ]; then
      echo "${storedSearch}: Session label column: $labelCol"
   fi

   # Find insert date column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l insert_date"
   fi
   insertDateCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l insert_date`
   if [ $verbose ]; then
      echo "${storedSearch}: Insert date column: $insertDateCol"
   fi

   # Find date column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l date"
   fi
   dateCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l date`
   if [ $verbose ]; then
      echo "${storedSearch}: Date column: $dateCol"
   fi

   # Find subject column
   if [ $verbose ]; then
      echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l subject_label"
   fi
   subjectCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l subject_label`
   if [ $verbose ]; then
      echo "${storedSearch}: Subject column: $subjectCol"
   fi

   # Find scanner column
   if [ $verbose ]; then
      echo "${storedSearch}: Attempting ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l scanner"
   fi
   #CT=scanner
   scannerCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l scanner`

   if [ $scannerCol -eq -1 ]; then
      if [ $verbose ]; then
         echo "${storedSearch}: Attempting ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l scanner_csv"
      fi
      #MR=scanner_csv
      scannerCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l scanner_csv`

      if [ $scannerCol -eq -1 ]; then
         if [ $verbose ]; then
            echo "${storedSearch}: Attempting ${UTILDIR}/getColumnForLabel.sh -s \"$storedSearchHeader\" -l scanner_name"
         fi
         #PET=scanner_name
         scannerCol=`${UTILDIR}/getColumnForLabel.sh -s "$storedSearchHeader" -l scanner_name`
         
         if [ $scannerCol -eq -1 ]; then
            if [ $verbose ]; then
               echo "${storedSearch}: Unable to find scanner header"
            fi
         fi
      fi
   fi
   if [ $verbose ]; then
      echo "${storedSearch}: Scanner column: $scannerCol"
   fi

   # Look through results from the stored search
   IFS=$'\n'
   for session in $sessionList; do
      if [ $verbose ]; then
         echo "session=$session"
      fi
      unset IFS

      sessionLabel=`echo ${session} | cut -d ',' -f ${labelCol} | cut -d '"' -f 2`
      scanner=`echo ${session} | cut -d ',' -f ${scannerCol} | cut -d '"' -f 2`

      # If no scanner is provided, no way to match the session data
      if [ -z "$scanner" ]; then
         if [ $verbose ]; then
            echo "${storedSearch}->${sessionLabel}: No scanner data provided."
         fi
         continue
      else
         if [ $verbose ]; then
            echo "${storedSearch}->${sessionLabel}: Scanner data found, ${scanner}."
         fi
      fi

      project=`echo ${session} | cut -d ',' -f ${projectCol} | cut -d '"' -f 2`
      projectAliases=`echo ${session} | cut -d ',' -f ${projectAliasesCol} | cut -d '"' -f 2`
      subject=`echo ${session} | cut -d ',' -f ${subjectCol} | cut -d '"' -f 2`
      scanDate=`echo ${session} | cut -d ',' -f ${dateCol} | cut -d '"' -f 2`
      uploadInsertionDate=`echo ${session} | cut -d ',' -f ${insertDateCol} | cut -d '"' -f 2`
      uploadInsertionDate=`echo "$uploadInsertionDate" | cut -d '.' -f 1`

      CCIR_ID=`echo ${project} | grep '^CCIR-[0-9]\{5\}'`

      if [ $verbose ]; then
         echo "${storedSearch}->${sessionLabel}: Verifying valid alias: ${project}"
      fi
      IFS=' '
      if [ -z "$CCIR_ID" ]; then
         echo "*${projectAliases}*"
         for projectAlias in $projectAliases; do
            unset IFS
         
            CCIR_ID=`echo ${projectAlias} | grep '^CCIR-[0-9]\{5\}'`
            if [ $verbose ]; then
               echo "${storedSearch}->${sessionLabel}: Verifying valid alias: ${projectAlias}"
            fi
            if [ ! -z "$CCIR_ID" ]; then
               break
            fi

            IFS=' '
         done
      fi

      # Loop through the scanner list file and write data to the matching scanner group(s)
      IFS=$'\n'
      for scannerLine in $scannerFileList; do
         unset IFS
         scannerLineClean=`echo "${scannerLine}" | awk -F'##' '{ print $1 }'`

         scannerGroup=`echo ${scannerLineClean} | awk -F'::' '{ print $1 }'`
         scannerGroup=`echo ${scannerGroup} | sed 's/^ *//g'`

         scannerList=`echo ${scannerLineClean} | awk -F'::' '{ print $2 }'`
         scannerList=`echo ${scannerList} | sed 's/^ *//g'`
         
         if [ $timeStamp ]; then
            reportFileName=${scannerGroup}_${timeStamp}.csv
         else
            reportFileName=${scannerGroup}.csv
         fi
 
         # Search for the session's scanner in the scanner group's scanner list
         IFS=","
         for scannerName in $scannerList; do
            unset IFS

            if [ $verbose ]; then
               echo "checking if ${scannerName} == ${scanner}"
            fi
            if [ "$scannerName" == "$scanner" ]; then

               scanInfoFileName=".scanInfo_${storedSearch}"

               if [ $verbose ]; then
                  echo "Executing REST request for scan details: curl -k -b JSESSIONID=$JSESSION \"${host}/REST/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/resources?all=true&format=csv&file_stats=true\""
                  echo "Writing to $WRKDIR/${scanInfoFileName}"
               fi

               curl -k -b JSESSIONID=$JSESSION "${host}/REST/projects/${project}/subjects/${subject}/experiments/${sessionLabel}/resources?all=true&format=csv&file_stats=true"  > $WRKDIR/${scanInfoFileName}

               if [ $verbose ]; then
                  echo "Checking for bad results: grep -i \"<HTML>\" $WRKDIR/${scanInfoFileName}"
               fi
               badResults=`grep -i "<HTML>" $WRKDIR/${scanInfoFileName}`

               if [ -n "$badResults" ]; then
                  echo "Bad results found. Skipping session ${sessionLabel}"
                  continue
               fi

               resourcesHeader=`cat ${WRKDIR}/${scanInfoFileName} | head -1`
               resources=`cat ${WRKDIR}/${scanInfoFileName} | grep DICOM`
               
               if [ $verbose ]; then
                  echo "Resources Header: ${resourcesHeader}"
               fi

               # Find subject column
               if [ $verbose ]; then
                  echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$resourcesHeader\" -l file_count"
               fi
               fileCountCol=`${UTILDIR}/getColumnForLabel.sh -s "$resourcesHeader" -l file_count`
               if [ $verbose ]; then
                  echo "${storedSearch}: fileCountCol=${fileCountCol}"
               fi
            
               if [ $verbose ]; then
                  echo "${storedSearch}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$resourcesHeader\" -l cat_desc"
               fi
               fileDetailsCol=`${UTILDIR}/getColumnForLabel.sh -s "$resourcesHeader" -l cat_desc`
               if [ $verbose ]; then
                  echo "${storedSearch}: fileDetailsCol=${fileDetailsCol}"
               fi

               let totalFiles=0
               fileDescriptions=""

               IFS=$'\n'
               for resource in $resources; do
                  unset IFS

                  fileCount=`echo ${resource} | cut -d ',' -f ${fileCountCol} `
                  if [ $verbose ]; then
                     echo "File Count: ${fileCount}"
                  fi

                  let totalFiles=${totalFiles}+${fileCount}

                  fileDetail=`echo ${resource} | cut -d ',' -f ${fileDetailsCol} | cut -d '"' -f 2`
                  if [ $verbose ]; then
                     echo "File Detail: ${fileDetail}"
                  fi

                  fileDescriptions="${fileDescriptions}${fileDetail} (${fileCount}) "

                  IFS=$'\n'
               done
               unset IFS

               # Trim white spaces from start and end of file descriptions
               fileDescriptions=`echo ${fileDescriptions} | sed 's/^ *//g'`

               if [ $verbose ]; then
                  echo "${storedSearch}->${sessionLabel}: Writing to ${reportsDir}/${reportFileName}: archive,${project},${CCIR_ID},${subject},${sessionLabel},${scanDate},${uploadInsertionDate},${scanner},${totalFiles},${fileDescriptions}"
               fi
               echo "archive,${project},${CCIR_ID},${subject},${sessionLabel},${scanDate},${uploadInsertionDate},${scanner},${totalFiles},${fileDescriptions}" >> $reportsDir/$reportFileName

               break
            fi
         IFS=","
         done
         unset IFS

         IFS=$'\n'
      done
      unset IFS

      IFS=$'\n'
   done
   unset IFS

   IFS=','
done
unset IFS
