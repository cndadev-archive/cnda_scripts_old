#!/bin/bash

# Generate a file for each scanner. Then combine then into attachments for each user so we only need to run through the queries once.

# Pull data from the prearchive and generate a report file for each scanner group.
#
# For this script we are only pulling data for CCIR projects

SCRIPTNAME=Weekly_Prearchive_Report

# Set default working directory
CURDIR=`pwd`
WRKDIR=$CURDIR

# Location of the getColumnForLabel.sh script
UTILDIR=/data/CNDA/cron/utilities/

function usage
{
   echo ""
   echo "${SCRIPTNAME} generates reports with sessions in the prearchive."
   echo ""
   echo "Usage: ${SCRIPTNAME}.sh -H host -U username [-P password] -s scannersfile -r reportdir [-d daysago -w workingDir -v]"   
   echo "   REQUIRED"
   echo "-H || --host          Host to connect to in format:  http(s)://yoursite.domain.ext"
   echo "-U || --userid     User id to make connection to CNDA."
   echo "-s || --scannersfile  File containing the scanner group, scanner list and email recipient list."
   echo "-r || --reportsdir    Directory for storing report files"
   echo ""
   echo "   OPTIONAL"  
   echo "-P || --password     Password can be provided on the command line."    
   echo "-w || --workdir      Location of the report generation scripts."
   echo "-d || --daysago        Number of days ago upload date must fall within."
   echo "-T || --timeStamp      A timestamp to add to the end of the file name of the report."
   echo "-v || --verbose        Run script in verbose mode."
   echo ""
}

if [ $# == 0 ]
then
   echo ""
      echo "No arguments provided"
      echo ""
      usage
   exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
   case $1 in
      -H | --host )
         shift
         host=$1
         ;;
      -U | --userid )
         shift
         userid=$1
         ;;
      -s | --scannersfile )
         shift
         scannerFile=$1
         ;;
      -r | --reportsdir )
         shift
         reportsDir=$1
         ;;
      -P | --password )
         shift
         password=$1
         ;;
      -d | --daysago )
         shift
         daysAgo=$1
         ;;
      -w | --workdir )
         shift
         workdir=$1
         ;;
      -v | --verbose )
         verbose=1
         ;;
      -T | --timeStamp )
         shift
         timeStamp=$1
         ;;
      -h | --help )
         usage
         exit 1
    esac
    shift
done

# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
   echo "Host required."
   echo ""  
   usage
   exit 1
fi

# Subject is required. If not, echo error message and usage output.
if [ -z "$userid" ]; then
   echo "User id required."
   echo ""  
   usage
   exit 1
fi

# Scanner file is required. If not, echo error message and usage output.
if [ -z "$scannerFile" ]; then
   echo "Scanner file is required."
   echo ""  
   usage
   exit 1
fi

# Location for generated report files is required. If not, echo error message and usage output.
if [ -z "$reportsDir" ]; then
   echo "Location for generated report files is required."
   echo ""  
   usage
   exit 1
fi

# Read in from the scanner list file
scannerFileList=`cat ${scannerFile}`

# If wrkdir file supplied, make sure it exists.
if [ -d "$workdir" ]; then
    WRKDIR=$workdir
else 
    echo Work directory \"${workdir}\" does not exist, using \"${WRKDIR}\" instead
fi

# If verbose, then indicate that (can copy and paste this snippet throughout code)
if [ $verbose ]; then
   echo "Running in verbose mode"
fi

# Get session id
if [ -z "$password" ]; then
   JSESSION=`curl -k -u ${userid} ""${host}/REST/JSESSION""`
else  
   JSESSION=`curl -k -u ${userid}:${password} ""${host}/REST/JSESSION""`
fi
responseCount=`echo $JSESSION | wc -m`

if [ $responseCount -gt 33 ]; then
   echo ${host} log "in" failed. Please check your password and retry.
   exit 1
fi  
if [ $verbose ]; then
   echo "You were logged onto ${host} successfully"
fi 

# convert the number of days ago into seconds ago
if [ ! -z "$daysAgo" ]; then
   dateNumDaysAgo=`date +%Y-%m-%d --date="${daysAgo} days ago"`
   secondsAgo=`date +%s -d "${dateNumDaysAgo}"`
fi

#Check if directory for report files exists and create it if it doesn't.
if [ ! -e "$reportsDir" ]; then
   if [ $verbose ]; then
      echo "Directory ${reportsDir} created."
   fi
   mkdir $reportsDir
else
   if [ $verbose ]; then
      echo "Directory ${reportsDir} already exists."
   fi
fi

# Pull list of CCIR projects
if [ $verbose ]; then
   echo Executing REST request: "curl -k -b JSESSIONID=${JSESSION} \"${host}/REST/projects?alias=*ccir*&columns=alias&format=csv\" | tail -n+2"
fi
projectList=`curl -k -b JSESSIONID=${JSESSION} "${host}/REST/projects?alias=*ccir*&columns=alias&format=csv" | tail -n+2`

# Loop through the CCIR projects
IFS=$'\n'
for projectListLine in $projectList; do
   unset IFS

   projectID=`echo $projectListLine | cut -d '"' -f 2 | cut -d '"' -f 1`

   badChar=`echo "${projectID}" | grep '[^a-zA-Z0-9_/-]'`
   if [ -n "$badChar" ]; then
      echo "Illegal characters found in project ID ${projectID}."
      continue;
   fi

   # If the project id provided is the same as the last project, skip it
   if [ "$currentProject" != "$projectID" ]; then
      currentProject=$projectID
   else
      continue
   fi

   CCIR_ID=`echo ${projectID} | grep '^CCIR-[0-9]\{5\}'`
   if [ -z "$CCIR_ID" ]; then
      if [ $verbose ]; then
         echo Executing REST request "curl -sk -b JSESSIONID=${JSESSION} \"${host}/REST/projects?columns=alias&ID=${projectID}&format=csv\""
      fi
      projectResults=`curl -sk -b JSESSIONID=${JSESSION} "${host}/REST/projects?columns=alias&ID=${projectID}&format=csv"`

      projectAliasesHeader=`echo "${projectResults}" | head -1`
      projectAliasesList=`echo "${projectResults}" | tail -n+2`

      if [ $verbose ]; then
         echo "${projectID}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$projectAliasesHeader\" -l alias"
      fi
      aliasCol=`${UTILDIR}/getColumnForLabel.sh -s "$projectAliasesHeader" -l alias`

      if [ $verbose ]; then
         echo "${projectID}: Alias column: $aliasCol"
      fi

      IFS=$'\n'
      for projectRow in $projectAliasesList; do
         unset IFS

         alias=`echo $projectRow | cut -d ',' -f ${aliasCol} |  cut -d ',' -f 1`
         alias=`echo $alias | cut -d '"' -f 2 |  cut -d '"' -f 1`

         CCIR_ID=`echo "${alias}" | grep '^CCIR-[0-9]\{5\}'`
         if [ ! -z "$CCIR_ID" ]; then
            break
         fi

         IFS=$'\n'
      done
   fi


   if [ $verbose ]; then
      echo "${projectID}: Executing REST request: curl -k -b JSESSIONID=${JSESSION} \"${host}/data/prearchive/projects/${currentProject}?format=csv\" | tail -n+2"
   fi

   sessionResults=`curl -sk -b JSESSIONID=${JSESSION} "${host}/data/prearchive/projects/${projectID}?format=csv"`

   sessionHeader=`echo "${sessionResults}" | head -1`
   sessionList=`echo "${sessionResults}" | tail -n+2`

   if [ $verbose ]; then
      echo "${projectID}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$sessionHeader\" -l subject"
   fi
   subjectCol=`${UTILDIR}/getColumnForLabel.sh -s "$sessionHeader" -l subject`

   if [ $verbose ]; then
      echo "${projectID}: Subject column: $subjectCol"
   fi

   if [ $verbose ]; then
      echo "${projectID}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$sessionHeader\" -l scan_date"
   fi
   scanCol=`${UTILDIR}/getColumnForLabel.sh -s "$sessionHeader" -l scan_date`
   if [ $verbose ]; then
      echo "${projectID}: Scan Date column: $scanCol"
   fi

   if [ $verbose ]; then
      echo "${projectID}: Executing ${UTILDIR}/getColumnForLabel.sh -s \"$sessionHeader\" -l uploaded"
   fi
   uploadCol=`${UTILDIR}/getColumnForLabel.sh -s "$sessionHeader" -l uploaded`
   if [ $verbose ]; then
      echo "${projectID}: Upload Date column: $uploadCol"
   fi

   IFS=$'\n'
   for session in $sessionList; do  
      unset IFS

      subject=`echo $session | cut -d ',' -f ${subjectCol} |  cut -d ',' -f 1`
      subject=`echo $subject | cut -d '"' -f 2 |  cut -d '"' -f 1`

      scanDate=`echo $session | cut -d ',' -f ${scanCol} |  cut -d ',' -f 1`
      scanDate=`echo $scanDate | cut -d ' ' -f 1`

      uploadDate=`echo $session | cut -d ',' -f ${uploadCol} |  cut -d ',' -f 1`
      # Cut off time. It's 00:00:00 and not useful.
      uploadDate=`echo "$uploadDate" | cut -d '.' -f 1`

      secondsUploadDate=`date +%s -d "${uploadDate}"`

      # If daysAgo is provided, skip any session older than that many days
      if [ ! -z "$daysAgo" ]; then
         if [ $secondsUploadDate -lt $secondsAgo ]; then
            continue
         fi
      fi

      url=`echo $session | cut -d ',' -f 12 |  cut -d ',' -f 1`
      url=`echo $url | cut -d '"' -f 2 |  cut -d '"' -f 1`

      if [ $verbose ]; then
         echo "${projectID}: Executing REST request: curl -k -b JSESSIONID=${JSESSION} \"${host}/data${url}?format=xml\""
      fi
      sessionDetails=`curl -sk -b JSESSIONID=${JSESSION} "${host}/data${url}?format=xml"`

      scanner=`echo ${sessionDetails} | grep -o -m 1 -E  '<xnat:scanner manufacturer=".*" model=".*">.*<\/xnat:scanner>' | cut -d'>' -f2 | cut -d'<' -f1`

      scanner=`echo $scanner | sed 's/^ *//g'`

      # If no scanner is provided, no way to match the session data
      if [ -z "$scanner" ]; then
         continue
      fi

      #Verify scanner for the current session is in the scanner list file
      scannerFound=0

      IFS=$'\n'
      for scannerFileLine in $scannerFileList; do
         unset IFS      

         scannerFileLineClean=`echo "${scannerFileLine}" | awk -F'##' '{ print $1 }'`

         scannerList=`echo ${scannerFileLineClean} | awk -F'::' '{ print $2 }'`
         scannerList=`echo ${scannerList} | sed 's/^ *//g'`

         IFS=","
         for scannerName in $scannerList; do
            unset IFS

            if [ "$scannerName" == "$scanner" ]; then
               scannerFound=1
               break
            fi

            IFS=','
         done
         
         IFS=$'\n'
      done
      unset IFS

      if [ $scannerFound -eq 0 ]; then
         continue
      fi

      label=`echo ${sessionDetails} | grep -o -m 1 -E  'label=".*"' | cut -d'"' -f2 | cut -d'"' -f1`

      if [ $verbose ]; then
         echo "${projectID}->${label}: Executing REST request: curl -k -b JSESSIONID=${JSESSION} \"${host}/data${url}/scans?format=csv\" | tail -n+2"
      fi
      scanList=`curl -sk -b JSESSIONID=${JSESSION} "${host}/data${url}/scans?format=csv" | tail -n+2`

      scanInfo=""
      totalFileCount=0

      # Generate the scanner details data
      IFS=$'\n'
      for scan in $scanList; do
         unset IFS

         scanID=`echo $scan | cut -d"," -f1`
         scanID=`echo $scanID |  cut -d'"' -f2 | cut -d'"' -f1`

         series_description=`echo $scan | cut -d"," -f3 | cut -d"," -f1`
         series_description=`echo $series_description | cut -d'"' -f2 | cut -d'"' -f1`

         if [ $verbose ]; then
            echo "${projectID}->${label}->${scanID}: Executing REST request: curl -k -b JSESSIONID=${JSESSION} \"${host}/data${url}/scans/${scanID}/resources?format=csv\" | tail -n+2"
         fi
         scanResourceList=`curl -sk -b JSESSIONID=${JSESSION} "${host}/data${url}/scans/${scanID}/resources?format=csv" | tail -n+2`

         IFS=$'\n'
         for scanResource in $scanResourceList; do
            unset IFS

            file_count=`echo $scanResource | cut -d"," -f2 |cut -d"," -f1`
            file_size=`echo $scanResource | cut -d"," -f3`

            totalFileCount=$(($totalFileCount+$file_count))

            scanInfo="${scanInfo}${series_description} (${file_count}) "

            IFS=$'\n'
         done        
         IFS=$'\n'
      done
      unset IFS

      scanInfo=`echo ${scanInfo} | sed 's/^ *//g'`

      # Loop through the scanner list file and write data to the matching scanner group(s)
      IFS=$'\n'
      for scannerLine in $scannerFileList; do
         unset IFS

         scannerLineClean=`echo "${scannerLine}" | awk -F'##' '{ print $1 }'`

         scannerGroup=`echo ${scannerLineClean} | awk -F'::' '{ print $1 }'`
         scannerGroup=`echo ${scannerGroup} | sed 's/^ *//g'`

         scannerList=`echo ${scannerLineClean} | awk -F'::' '{ print $2 }'`
         scannerList=`echo ${scannerList} | sed 's/^ *//g'`
         
         if [ $timeStamp ]; then
            reportFileName=${scannerGroup}_${timeStamp}.csv
         else
            reportFileName=${scannerGroup}.csv
         fi

         # Search for the session's scanner in the scanner group's scanner list
         IFS=","
         for scannerName in $scannerList; do
            unset IFS

            if [ "$scannerName" == "$scanner" ]; then
               if [ $verbose ]; then
                  echo "${projectID}->${label}: Writing to ${reportsDir}/${reportFileName}: prearchive,$projectID,$CCIR_ID,$subject,$label,$scanDate,$uploadDate,$scanner,$totalFileCount,$scanInfo"
               fi
               echo "prearchive,$projectID,$CCIR_ID,$subject,$label,$scanDate,$uploadDate,$scanner,$totalFileCount,$scanInfo" >> $reportsDir/$reportFileName

               break
            fi

            IFS=','
         done
         unset IFS

         IFS=$'\n'
      done
      unset IFS

      IFS=$'\n'
   done
   unset IFS

   IFS=$'\n'
done

unset IFS
