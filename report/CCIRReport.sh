#!/bin/bash
#set -x # For debugging purposes

# Unaltered by you, this script breaks. The script is essentially a skeleton
#  of many of the tools and methods often needed in CNDA scripts
#
# Features needed:
# 1.  Add language to find and parse .xnatPass file 

SCRIPTNAME=CCIRReport
CURDIR=`pwd`
LOGSUBDIR=logs
WRKDIR=$CURDIR
UTILDIR=/data/nil-bluearc/marcus/CNDA_TOOLS/CCIR_REPORT

# 

# Usage output for explaining how to use the program
function usage
{
	echo ""
        echo "${SCRIPTNAME} generates reports with scan file counts for the CCIR."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh (OPTIONS) [-H host -U username -P password] -o outputDataFile -w workingDir -t pattern -v verbose"	
	echo "   REQUIRED"
	echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
	echo "-U || --userid          User id to make connection."
        echo "                        User id and host are optional if .xnatPass file is used."
	echo ""
	echo "   OPTIONAL"  
        echo "-P || --password        Password can be provided on the command line, in the"
        echo "                        .xnatPass file or when prompted by the system."    
	echo "-o || --output          Path and filename to which to export data."
        echo "-w || --workdir         Working directory for execution of this project.  This is the"
        echo "                        to which intermediate or temporary work products and logs are saved."
        echo "-t || --pattern         Stored search id"
        echo "-v || --verbose         Run script in verbose mode."
        echo ""
}

#skipRows=3 # Variable that determines how many lines to skip in REST request returned output.

# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	echo ""
        echo "no arguments!!!"
        echo ""
        usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
                        host=$1
			;;
		-U | --userid )
			shift
			userid=$1
			;;
		-P | --password )
			shift
			password=:$1
			;;
		-o | --output )
			shift
			outputfile=$1
			;;
		-w | --workdir )
			shift
			workdir=$1
			;;
                -t | --pattern )
                        shift
			pattern=$1
			;;
                -v | --verbose )
			verbose=1
			;;
		-h | --help )
			usage
			exit 1
    esac
    shift
done


# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
	echo "Host required."
	echo ""	
	usage
	exit 1
fi

# Subject is required. If not, echo error message and usage output.
if [ -z "$userid" ]; then
	echo "User id required."
	echo ""	
	usage
	exit 1
fi

# If output file supplied, make sure it exists.
if [ ! -z "$output" ]; then
	if [ ! -s "$outputfile" ]; then
            echo "The output file you specified either does not exist or is empty."
            echo ""
            usage
            exit 1
	fi
fi

# If wrkdir file supplied, make sure it exists.
if [ -d "$wrkdir" ]; then
    WRKDIR=$wrkdir
else 
    echo ${wrkdir} does not exist, using $WRKDIR instead
fi


# If verbose, then indicate that (can copy and paste this snippet throughout code)
if [ $verbose ]; then
        echo "Running in verbose mode"
fi


# Get session id
JSESSION=`curl -k -u ${userid}${password} ""${host}/REST/JSESSION""` 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo ${host} log "in" failed. Please check your password and retry.
  exit 1
fi  
if [ $verbose ]; then
  echo "You were logged onto ${host} successfully"
fi                                                      


if [ $verbose ]; then
     echo Executing REST request: "curl -k -b JSESSIONID=$JSESSION \"${host}/data/search/saved/${pattern}/results?format=csv\" > ${WRKDIR}/.storedSearch"
fi         
# Get StoredSearch specified as pattern    
curl -k -b JSESSIONID=$JSESSION "${host}/data/search/saved/${pattern}/results?format=csv" > ${WRKDIR}/.storedSearch


# Get the line count     
IN_LINECOUNT=`wc -l ${WRKDIR}/.storedSearch | awk '{print $1}'`
if [ $verbose ]; then
      echo "Total line count:  $IN_LINECOUNT"
fi
 
# Get the top row from the file (for spreadsheet headers)
HEADERS=`cat ${WRKDIR}/.storedSearch | head -1`
if [ $verbose ]; then
      echo "Your file headers are:  $HEADERS"
fi


# Specify columns
PROJCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l project`
if [ $verbose ]; then
      echo "Project column: $PROJCOL"
fi

LABELCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l label` 
if [ $verbose ]; then
      echo "Label column: $LABELCOL"
fi

DATECOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l date` 
if [ $verbose ]; then
      echo "Date column: $DATECOL"
fi

SUBJCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l xnat_subjectdata_subject_label`
if [ $verbose ]; then
      echo "Subject column: $SUBJCOL"
fi

SCANNERCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l scanner` 
let SCANNERCOL=$SCANNERCOL+1
if [ $verbose ]; then
      echo "Scanner column: $SCANNERCOL"
fi

INSERTCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS -l insert_date` 
if [ $verbose ]; then
      echo "Insert column: $INSERTCOL"
fi

        
echo "\"Project\",\"Label\",\"Date\",\"Subject\",\"Scanner\",\"Upload Date\",\"Total Files\",\"Scan Info\"" > ${outputfile}    
if [ $verbose ]; then
      echo "Output file headers: \"Project\",\"Label\",\"Date\",\"Subject\",\"Scanner\",\"Upload Date\",\"Total Files\",\"Scan Info\""
fi
                                                    
# Loop to iterate through input file
# initialize counter (if header on file, then switch to i=1
let i=1

while [ "$i" -lt "${IN_LINECOUNT}" ]
do      
    let i=$i+1
    # Get variable from column 1 of the ith row in a csv file
    #variable=`awk 'NR=='$i'' "$<csv file>"  | sed -e 's/,/ /g;s/"//g' | awk '{print $1}'`
    #echo variable: $variable
    
    # Get variables
    curLine=`awk 'NR=='$i''  ${WRKDIR}/.storedSearch`
    project=$(echo $curLine | cut -d',' -f"$PROJCOL" | sed -e 's/"//g')
    if [ $verbose ]; then
         echo project: $project
    fi 
    label=$(echo $curLine | cut -d',' -f"$LABELCOL" | sed -e 's/"//g')
    if [ $verbose ]; then
         echo label: $label
    fi 
    date=$(echo $curLine | cut -d',' -f"$DATECOL" | sed -e 's/"//g')
    if [ $verbose ]; then
         echo date: $date
    fi 
    subject=$(echo $curLine | cut -d',' -f"$SUBJCOL" | sed -e 's/"//g')
    if [ $verbose ]; then
         echo subject: $subject
    fi     
    scanner=$(echo $curLine | cut -d',' -f"$SCANNERCOL" | sed -e 's/"//g')
    #scanner=echo $curLine | `sed -e 's/,/ /g;s/"//g'` | `awk '{print $1}'`
    if [ $verbose ]; then
         echo scanner: $scanner
    fi     
    insertdate=$(echo $curLine | cut -d',' -f"$INSERTCOL" | sed -e 's/"//g')
    if [ $verbose ]; then
         echo insertdate: $insertdate
    fi      
    scanagg="\"$project\",\"$label\",\"$date\",\"$subject\",\"$scanner\",\"$insertdate\""
    if [ $verbose ]; then
         echo Aggregated line so far: $scanagg
    fi
   
    if [ $verbose ]; then
         echo Executing REST request: "curl -k -b JSESSIONID=$JSESSION \"${host}/REST/projects/${project}/subjects/${subject}/experiments/${label}/resources?all=true&format=csv&file_stats=true\" | grep -v SNAPSHOTS > $WRKDIR/.scanInfo"
    fi
    curl -k -b JSESSIONID=$JSESSION "${host}/REST/projects/${project}/subjects/${subject}/experiments/${label}/resources?all=true&format=csv&file_stats=true" | grep -v SNAPSHOTS > $WRKDIR/.scanInfo 
    
    # Get the line count     
    IN_LINECOUNT2=`wc -l ${WRKDIR}/.scanInfo | awk '{print $1}'` 
    if [ $verbose ]; then
      echo "Total series line count:  $IN_LINECOUNT2"
    fi
    
    # Get the top row from the file (for spreadsheet headers)
    HEADERS2=`cat ${WRKDIR}/.scanInfo | head -1`
    if [ $verbose ]; then
      echo "Series headers:  $HEADERS2"
    fi

    
    # Specify columns
    CATIDCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS2 -l cat_id`
    if [ $verbose ]; then
      echo "Category id column:  $CATIDCOL"
    fi
    CATDESCCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS2 -l cat_desc`
    if [ $verbose ]; then
      echo "Category desc column:  $CATDESCCOL"
    fi 
    FILECOUNTCOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS2 -l file_count` 
    if [ $verbose ]; then
      echo "File count column:  $FILECOUNTCOL"
    fi 
    FILESIZECOL=`${UTILDIR}/getColumnForLabel.sh -s $HEADERS2 -l file_size` 
    if [ $verbose ]; then
      echo "File size column:  $FILESIZECOL"
    fi 

    # Sort by series #
    sed 1d ${WRKDIR}/.scanInfo > ${WRKDIR}/.scanInfoTmp
    /bin/sort -t , -k ${CATIDCOL},${CATIDCOL}n ${WRKDIR}/.scanInfoTmp > ${WRKDIR}/.scanInfoSorted
    # Headers will be last now, so don't look at that line
    let totalFiles=0
    seriesagg=\"
    let j=1
    while [ "$j" -lt "${IN_LINECOUNT2}" ]
    do      
        # Get variables
        curLine=`awk 'NR=='$j'' ${WRKDIR}/.scanInfoSorted`
        catid=$(echo $curLine | cut -d',' -f"$CATIDCOL" | sed -e 's/"//g')
        if [ $verbose ]; then
            echo catid: $catid
        fi
        catdesc=$(echo $curLine | cut -d',' -f"$CATDESCCOL" | sed -e 's/"//g')
        if [ $verbose ]; then
            echo catdesc: $catdesc
        fi
        filecount=$(echo $curLine | cut -d',' -f"$FILECOUNTCOL" | sed -e 's/"//g')
        if [ $verbose ]; then
            echo filecount: $filecount
        fi
        let totalFiles=${totalFiles}+${filecount}
        filesize=$(echo $curLine | cut -d',' -f"$FILESIZECOL" | sed -e 's/"//g')
        if [ $verbose ]; then
            echo filesize: $filesize    
        fi
        let filesize=${filesize}/1024
        seriesagg="${seriesagg}  ${catid} ${catdesc} (filecount ${filecount}) (${filesize} KB)"  
        let j=$j+1     
    done
    seriesagg=${seriesagg}\"
    scanagg=${scanagg},\"${totalFiles}\",${seriesagg}
    if [ $verbose ]; then
        echo Current scan aggregate: ${scanagg}
    fi    
    echo "${scanagg}" >> ${outputfile}
    rm $WRKDIR/.scanInfo
    rm $WRKDIR/.scanInfoTmp
    rm $WRKDIR/.scanInfoSorted      

done
# remove temp files
rm $WRKDIR/.storedSearch



cd $CURDIR
