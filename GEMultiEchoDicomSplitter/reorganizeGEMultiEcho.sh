#!/bin/bash

TMPFILE=./.tmpDicomList.txt
PROJECT=
ARCHIVEPATH=/data/CNDA/archive/${PROJECT}/arc001

# if scan type="EPI 4echo (physio)" then

LIST=$1

# iterate input file
while read line 
do

session=`echo ${line} | cut -d, -f2`
if [ -z ${session} ]; then
   echo Please set the session 
   exit 1
fi

scanspath=${ARCHIVEPATH}/${session}/SCANS

scanlist=`echo ${line} | cut -d, -f3`
if [ -z "${scanlist}" ]; then
   echo Please set the scanlist
   exit 1
fi

# iterate scans in input file
for scan in ${scanlist}
do

echo Beginning scan ${scan}

max=0
min=10000000

echo min=${min} max=${max}

if [ -z ${scan} ]; then
   echo Please set the scan number
   exit 1
fi

if [ ! -d ${scanspath}/{scan}001/DICOM ]; then
   mkdir --parents ${scanspath}/${scan}001/DICOM
fi

if [ ! -d ${scanspath}/${scan}002/DICOM ]; then
   mkdir --parents ${scanspath}/${scan}002/DICOM
fi

if [ ! -d ${scanspath}/${scan}003/DICOM ]; then
   mkdir --parents ${scanspath}/${scan}003/DICOM
fi

if [ ! -d ${scanspath}/${scan}004/DICOM ]; then
   mkdir --parents ${scanspath}/${scan}004/DICOM
fi

ls ${scanspath}/${scan}/DICOM/*.dcm | head -137 > ${TMPFILE}

while read line
do
    val=`echo $line | cut -d. -f7`
    echo val: $val
    if [ "$val" -lt "$min" ]; then
       export min=$val;
    fi
    if [ "$val" -gt "$max" ]; then
       export max=$val
    fi
    echo new min: $min new max: $max
done < ${TMPFILE}

# DELETE ME
read y

echo again min:  $min

echo1=$((min + 33))
echo2=$((echo1 + 34))
echo3=$((echo2 + 34))
echo4=$((echo3 + 34))

echo echo1 $echo1
echo echo2 $echo2
echo echo3 $echo3
echo echo4 $echo4

#DELETE ME
read y

# split echo 1
for i in `seq ${min} ${echo1}`;
do
   echo ${i}
   cp ${scanspath}/${scan}/DICOM/*.${i}.dcm ${scanspath}/${scan}001/DICOM
done

# split echo 2
start=$((echo1 + 1))
echo $start $echo2
for i in `seq ${start} ${echo2}`;
do
   echo ${i}
   cp ${scanspath}/${scan}/DICOM/*.${i}.dcm ${scanspath}/${scan}002/DICOM
done

# split echo 3
start=$((echo2 + 1))
echo $start $echo3
for i in `seq ${start} ${echo3}`;
do
   echo ${i}
   cp ${scanspath}/${scan}/DICOM/*.${i}.dcm ${scanspath}/${scan}003/DICOM
done

# split echo 4
start=$((echo3 + 1))
echo $start $echo4
for i in `seq ${start} ${echo4}`;
do
   echo ${i}
   cp ${scanspath}/${scan}/DICOM/*.${i}.dcm ${scanspath}/${scan}004/DICOM
done

rm ${TMPFILE}

echo ${scanspath}/${scan}001/DICOM
ls ${scanspath}/${scan}001/DICOM/*.dcm | wc -l
echo ${scanspath}/${scan}002/DICOM
ls ${scanspath}/${scan}002/DICOM/*.dcm | wc -l
echo ${scanspath}/${scan}003/DICOM
ls ${scanspath}/${scan}003/DICOM/*.dcm | wc -l
echo ${scanspath}/${scan}004/DICOM
ls ${scanspath}/${scan}004/DICOM/*.dcm | wc -l

done # for loop
done < ${LIST} # while loop
