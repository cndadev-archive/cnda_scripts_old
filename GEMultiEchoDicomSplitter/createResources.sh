#!/bin/bash

#### I put this script together very quickly after my last multi-echo split.  STILL NEEDS TO BE TESTED!! #######
SITE=
PROJECT=
USER=$1
LIST=$2

USAGE="Usage: ./createResources.sh USER LIST"

if [ -z ${USER} ]; then
   echo ${USAGE}
   echo Please provide the USER id
   exit 1
fi
if [ -z ${LIST} ]; then
   echo ${USAGE}
   echo Please provide the list of sessions and scans
   exit 1
fi

JSESSION=`curl -k -u ${USER} "${SITE}/data/JSESSION"`
echo $JSESSION

# iterate input file
while read line
do

subject=`echo ${line} | cut -d, -f1`
echo ${subject}
if [ -z ${subject} ]; then
   echo Please set the subject
   exit 1
fi

session=`echo ${line} | cut -d, -f2`
echo ${session}
if [ -z ${session} ]; then
   echo Please set the session
   exit 1
fi

scanspath=${ARCHIVEPATH}/${session}/SCANS

scanlist=`echo ${line} | cut -d, -f3`
echo ${scanlist}
if [ -z "${scanlist}" ]; then
   echo Please set the scanlist
   exit 1
fi

# iterate scans in input file
for scan in ${scanlist}
do

#Add new scans
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}001?xnat:mrScanData/type=EPI%204echo%20(physio)&series_description=EPI%204echo%20(physio)&quality=usable"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}002?xnat:mrScanData/type=EPI%204echo%20(physio)&series_description=EPI%204echo%20(physio)&quality=usable"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}003?xnat:mrScanData/type=EPI%204echo%20(physio)&series_description=EPI%204echo%20(physio)&quality=usable"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}004?xnat:mrScanData/type=EPI%204echo%20(physio)&series_description=EPI%204echo%20(physio)&quality=usable"

# Add DICOM resources for new scans
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}001/resources/DICOM?format=DICOM&content=RAW"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}002/resources/DICOM?format=DICOM&content=RAW"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}003/resources/DICOM?format=DICOM&content=RAW"
curl -k -b JSESSIONID=${JSESSION} -X PUT "${SITE}/data/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}/scans/${scan}004/resources/DICOM?format=DICOM&content=RAW"

done # end for loop

# Refresh session resources as daemon so JSESSION won't time out
curl -k -b JSESSIONID=${JSESSION} -X POST "${SITE}/data/services/refresh/catalog?options=populateStats%2Cappend%2Cdelete%2Cchecksum&resource=/archive/projects/${PROJECT}/subjects/${subject}/experiments/${session}"&

done < ${LIST} # while loop
