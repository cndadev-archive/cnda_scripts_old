#!/usr/local/bin/python2.7

# Script to sort data from a multi-echo enabled version of GE's EPI
# sequence.
#
# Algorithm for sorting is based on tags found by Wen-Ming Luh in GE's
# DICOM headers, and shell script also provided by Wen-Ming.
#
#                                                  Vinai Roopchansingh
#                                                     FMRIF/NIMH/NIH
#                                                        2013.01.31

import os, sys, getopt, subprocess
import time, signal
import numpy
import shutil
# import dicom



nEchoes   = 3
nSlices   = 1
fileExt   = "dcm"
workDir   = "."



def printHelp (args):

   print ""
   print "   Usage: " + args[0] + "   -nz nSlices   -x ext" #   -d dir"
   print ""
   print "    where   nSlices   = number of slices"
   print "            ext       = file extension (default: \"dcm\")."
   # print "            dir       = directory to sort (default: \".\", i.e. the"
   # print "                        current working directory)."
   print ""
   print "   This script requires either the PyDicom egg to be installed, or"
   print "   the AFNI program dicom_hinfo in the $PATH of the account running"
   print "   this program.  Right now, this script is configured to use the"
   print "   latter option, though the code for the other option is commented"
   print "   and can be uncommented as needed."
   print ""



def processOptions (argv):

   global nSlices

   if ((len(argv) <= 1) or ("-h" in argv) or ("--h" in argv)
                        or ("-help" in argv) or ("--help" in argv)):
      printHelp(argv)
      exit (0)

   for i in range(len(argv)):

      if argv[i] in ("-nz"):
         i += 1
         nSlices = int(argv[i])
         print ""
         sys.stderr.write ('Setting number of slices to ' + str(nSlices) + '\n')

      if argv[i] in ("-x"):
         i += 1
         fileExt = argv[i]
         print ""
         sys.stderr.write ('Processing files ending in ' + fileExt + '\n')

      if argv[i] in ("-d"):
         i += 1
         workDir = argv[i]
         print ""
         sys.stderr.write ('Processing files in directory ' + workDir + '\n')



def returnTagValue (dicomFile, tagValuesRequired):

   # If we were on a system where we have access to PyDicom, we could use
   # this ...
   #
   # while True:

      # try:
         # dicomFilePtr = dicom.read_file (dicomFile)
      # except InvalidDicomError:
         # continue

      # break

   # print "All available fields are " + str (dicomFilePtr.dir(""))

   # print "Series description is " + str (dicomFilePtr.SeriesDescription)

   # print "Total number of slices is " + str(dicomFilePtr[0x20,0x1002].value)

   # However, on the GE console, since I don't want to install Python Eggs,
   # I can also compile the dicom_hinfo module when I compile Dimon, and
   # have that available ...

   while True:
      try:
         tagValueObj = subprocess.Popen (['dicom_hinfo',
                                          '-tag', tagValuesRequired,
                                          dicomFile], shell=False,
                                          stdout=subprocess.PIPE)
      except OSError:
         continue

      break

   tagValueString = tagValueObj.stdout.read()

   return (tagValueString.split()[1])



if __name__ == '__main__':

   processOptions (sys.argv[0:])

   numIndices = 0 # number of unique index values = nSlices * number of echoes

   os.chdir (workDir)

#   allFileNames = dict ([(f, None) for f in os.listdir ('.') if
#                                                   f.endswith ("." + fileExt)])
#  modified by KL
   allFileNames = dict ([(f, None) for f in os.listdir ('.') if f.startswith('i')])
   sliceIndexList = list()

   # print "First file is " + allFileNames.keys()[0]

   # Find all image indices in private tag.  The number of these tags ==
   # number of slices * number of echoes.
   numIndices = int(returnTagValue (allFileNames.keys()[0], "0020,1002"))

   print ""
   print "Number of slices * echoes is " + str (numIndices)

   print ""
   print "Getting list of slices * echoes indices"

   fileCount = 0
   # imageInstanceList = list()
   while (len(sliceIndexList) < numIndices):

      sliceIndex = returnTagValue (allFileNames.keys()[fileCount], "0019,10a2")

      # nRepetitions = returnTagValue (file2Process, "0020,0105")

      if sliceIndex not in sliceIndexList:
         sliceIndexList.append (sliceIndex)
         sliceIndexList.sort ()

         # print "Slice index for this  slice  is  " + str (sliceIndex)

         # imageInstance = returnTagValue (allFileNames.keys()[fileCount],
                                         # "0020,0013")

         # imageInstanceList.append (int(imageInstance))
         # imageInstanceList.sort ()

         # print "Image instance for this slice is " + str (imageInstance)

      fileCount += 1

   # end of while loop to build slice index list
   # print "Length of slice index list is " + str (len(sliceIndexList))

   # print ""
   # print "Sorted slice index list is " + str (sliceIndexList)
   # print ""
   # print "Sorted image instance list is " + str (imageInstanceList)

   # Figure of number of echoes collected, and regroup indices by echo.
   nEchoes = numIndices / nSlices
   sliceIndexList = numpy.reshape (sliceIndexList, [nEchoes, nSlices])

   # print "List of indices is " + str (sliceIndexList)

   echoDirectoriesList = list()
   for dirExt in range (0, nEchoes):
      dirName = "echo_%03d_dir" % (dirExt + 1)
      os.mkdir (dirName)
      echoDirectoriesList.append (dirName)

   echoDirectoriesList.sort()

   print ""
   print "Sorting images into echo sub-directories"

   imageCount = 0
   imageInstanceUIDList = list ()
   for file2Process in allFileNames:
      # Should not be moving and sorting duplicated UIDs into sub-directories,
      # So check for this tag value.
      imageInstanceUID = returnTagValue (file2Process, "0002,0003")

      # If we do NOT have a duplicated UID (i.e. a unique image Instance UID),
      # then sort and move the file.
      if imageInstanceUID not in imageInstanceUIDList:
         imageInstanceUIDList.append (imageInstanceUID)

         sliceIndex = returnTagValue (file2Process, "0019,10a2")

         for dirExt in range (0, nEchoes):
            if (sliceIndex in sliceIndexList[dirExt]):
               dirName = "echo_%03d_dir" % (dirExt + 1)
               # shutil.copy2 (file2Process, dirName)
               shutil.move (file2Process, dirName)

      if (imageCount % 100 == 0):
         sys.stdout.write (". ")
         sys.stdout.flush ()

      imageCount += 1

#   print ""
#   print "Building AFNI data sets from data in echo sub-directories."

#   echoCount = 1
#   for edir in echoDirectoriesList:
##      dimonCommand  = "Dimon -infile_pattern \"" + edir + "/*." + fileExt + "\" "
#      dimonCommand  = "Dimon -infile_pattern \"" + edir + "/i*" + "\" "
#      dimonCommand += "-gert_create_dataset -quiet "
#      dimonCommand += "-gert_to3d_prefix echo_%03d_dataset" % echoCount 

#      print ""
#      print "Running command: " + dimonCommand

#      os.system(dimonCommand)

#      echoCount += 1
 

