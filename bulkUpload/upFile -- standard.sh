
#!/bin/bash
#################################
#Jeanette Cline -  CNDA file uploader script
#
#This file required the following in .bashrc file
#which increases the java heap size
#
# export _JAVA_OPTIONS=-Cmx5120m
################################

#receiver location -- the CNDA
DICOM_LOC="dicom://cnda.wustl.edu:8104/CNDA" 

#path for remap file
REMAP=$HOME/remap/DicomBrowser-1.5-SNAPSHOT/bin

#path for base anon script
BASE_ANON=$HOME/remap/dicom.das

#path for storing new anon script
#create temp directory, will be deleted when complete
mkdir $HOME/remap/temp
TEMP_ANON=$HOME/remap/temp

#location of data to be uploaded
#send in as parameter on call
#contains subject directories
BASEDIR=${1:-`pwd`}

#path for saving log file
LOG=$HOME/remap/uploadLog.txt

#project name hard coded here
#presumes all files are going to the same project
PROJECTNAME="UCSF_Test"

#loop over sessions
#Script presumes that the dir below base 
#has a single session in it
for session_dir in ${BASEDIR}/*; do	
	if [ ! -d $session_dir ]; 
	then
	       #using break here caused a loop problem, continue fixed this
               continue
	fi

	#session name is directory name
	#only need bottom directory, not whole path
	session=${session_dir##*/}

	#line 33
	#subject name is within session name
	#dash signals end of subject name	
	subject=${session%*-*}

	#print general info to log file:
	echo "Starting Session: ${session} Subject: $subject" >> $LOG

	#create an anon script for this project,subject,session
	#copy base anon script to it
	#the symbol $$ is a process ID, I think this is to make a random name
	#save to a temp location for cat to save to
	anon_script="${TEMP_ANON}/anon-$$.das"	

	
	if [ -f $BASE_ANON ]; then
		cp $BASE_ANON $anon_script
	fi
	
	#line 49
	cat >> $anon_script <<EOF

(0008,1030):="${PROJECTNAME}"
(0010,0010):="${subject}"
(0010,0020):="${session}"
(0010,4000):="AA:true"
(0012,0063):="CNDA Common Deidentification V01"
EOF
#had to put end of file here, was giving error when indented (unexpected end of file)




	#run remap and upload
	$REMAP/DicomRemap -d $anon_script -o $DICOM_LOC ${session_dir}

	#signal complete to log file
	echo "Completed: ${session_dir}" >> $LOG

	#can now delete the custom anon script
	rm -f $$anon_script

done

#empty and remove temp anon directory
rm -r ${TEMP_ANON}
