#!/bin/bash

#Start usage function
# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} pulls spreadsheet from CNDA Resources subdirectory and uses it to run the script adrc.sh."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh (OPTIONS) -H host -U userid -P password -K usernameFile -v verbose"	
	echo "   OPTIONAL" 
	echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
	#echo "-K || 				   File containing username and password"
	echo "-U || --userid          User id to make connection."
	echo "-P || --password        Password can be provided on the command line "
    echo "-v || --verbose         Run script in verbose mode."
    echo ""
}
#end usage function

echo "ADRC Run Program"

# Default settings
host="https://cnda-shadow01.nrg.mir"
MAIN=$HOME/biostatTest
spreadsheet="${MAIN}/inputFile.csv"
CONFG="${MAIN}/config.txt"

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
            host=$1
			;;
		-U | --userid )
			shift
			userid=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
	esac
    shift
done

	if [ $verbose ]; then
		echo "Host is: " ${host}
		echo "Userid is: " ${userid}
    fi

echo "Retrieving file from CNDA"
	
	
# Create a new jsession 
#JSESSION=`curl -u $userid -k ""${host}/REST/JSESSION""` 
JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""` 


# Pull spreadsheet labeled Clinical_data from CNDA under Resources/csv_upload save to variable spreadsheet
curl -b JSESSIONID=$JSESSION -k "${host}/data/archive/projects/ADRC/resources/csv_upload/files/Clinical_Data?format=csv" > ${spreadsheet}
	if [ $verbose ]; then
		echo "CSV file created." 
    fi

cd ${MAIN}

echo "Running Script"

# Run script using spreadsheet just acquired
./adrc.sh -H "${host}" -i "${spreadsheet}"
	if [ $verbose ]; then
		echo "Script launched." 
    fi
	
echo "ADRC Run Program Complete"

# Delete temp file and jsession
rm ${spreadsheet}
curl -X DELETE -b JSESSIONID=$JSESSION -k "${host}/REST/JSESSION"


