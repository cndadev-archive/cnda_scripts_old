#!/bin/bash


# 	$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
#
#	Script to upload a file to the Resources directory in CNDA
#   File is found on CNDA under File Manager for the Project (ADRC), then by experiment (which is the name of the subdirectory of Resources)
#   Jeanette Cline  October 2014
#
#   Required files:  
#					config.txt  -- location specified in MAIN variable
#	$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$


#Start usage function
function usage
{
	echo "${SCRIPTNAME} uploads a file to the Resources directory in CNDA."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh -H host -U username -i inputDataFile  -e experiment -l label"	
	echo "   REQUIRED" 	
	echo "-i || --input           Path and filename from which to import data"	
   
		echo "-U || --userid          User id to make connection."
			echo "                        User id and host are optional if .xnatPass file is used."
	    echo "-p || --project         Specified CNDA project (or comma-separated list of projects)."
		echo "-e || --expt            Name of the subdirectory of Resources in the CNDA to save to."	
		echo "-l || --label			  The label to use for the file in CNDA. "
        echo ""
}
#END usage function

#Argument assignment function
# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

while [ "$1" != "" ]; do
	case $1 in
		-i | --input )
			shift
			inputfile=$1
			;;
    esac
    shift
done

# Default values
project=ADRC
host="https://cnda-shadow01.nrg.mir"
expt="csv_upload"
label="Clinical_Data"
MAIN=$HOME/biostat
CONFG="${MAIN}/config.txt"

echo "Running Resource Uploader"


# Create a new jsession 
# Uses a file with username:password stored inside
JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""` 

# Upload file:
curl -b JSESSIONID=$JSESSION -X PUT -T $inputfile -k "${host}/data/archive/projects/${project}/resources/${expt}/files/${label}?inbody=true&overwrite=true"

echo "Resource Uploader Complete"