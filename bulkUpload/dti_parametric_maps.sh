#!/bin/bash

# Written for bulk upload for Jim Alexopoulos
# Written by Jenny Gurney


# WRKDIR: file repository where files are stored
WRKDIR=/data/nil-bluearc/marcus/BBI/dti2cnda
# CURDIR: directory where files will be worked with, placed into a zip file
CURDIR=/data/nil-bluearc/marcus/gkgurney/jimA
# CFGFILE:  column 1:<file or directory name> column2:<subject id> column 3:<experiment id>
CFGFILE=wu_premie_dti_parametric_maps_uploads_20100907.txt
# PROJECT: project to which the files will be uploaded
PROJECT=wu_premie
# USERID: XNAT user id
USERID=
# PASSWD: XNAT password
PASSWD=
# XNATHOST: XNAT host url
XNATHOST=https://cnda.wustl.edu
# RESTPATH: Path to XNAT REST client
RESTPATH=/data/cninds01/data2/nrg-tools/XNATRestClient

date=`date "+%Y%m%d"`
echo date: $date

lineCount=`wc -l $CFGFILE | awk '{print $1}'`
echo line count: $lineCount
            
let i=0
echo i: $i
while [ "$i" -lt "$lineCount" ]
do
  let i=$i+1
  echo i: $i

  #get file/directory name
  name1=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $1}'`
  echo name1: $name1
  #get subject
  name2=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $2}'`
  echo name2: $name2
  #get experiment
  name3=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $3}'`
  echo name3: $name3

  mkdir $CURDIR/dti_parametric_maps_"$name1"_$date

  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_Amplitude_on_Atlas_noStretch.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_Amplitude_on_Atlas_noStretch.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_on_Atlas_noStretch.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_on_Atlas_noStretch_RGB* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_on_Atlas_noStretch_frame2.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_on_Atlas_noStretch_frame2_x1000.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_NDBcREF_dti_on_Atlas_noStretch_frame2_x1000.4dint* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_mask.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_B0_n3.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_to_711-2Neonat_noStretch_t4 $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_to_711-2Neonat_t4 $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_xenc.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_xenc.mat $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_xenc_NDBcREF_dti.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/*_dwi_phased_xenc_NDBcREF_dti_Amplitude.4dfp* $CURDIR/dti_parametric_maps_"$name1"_"$date"
  /bin/cp -r -p $WRKDIR/$name1/whisker.m $CURDIR/dti_parametric_maps_"$name1"_"$date"

  zip -r $CURDIR/dti_parametric_maps_"$name1"_"$date".zip $CURDIR/dti_parametric_maps_"$name1"_"$date" 

  $RESTPATH -host $XNATHOST -u $USERID -p $PASSWD -m PUT -remote /REST/projects/$PROJECT/subjects/"$name2"/experiments/"$name3"/resources/dti_parametric_maps/files/dti_parametric_maps_"$name1"_"$date".zip -local "$CURDIR"/dti_parametric_maps_"$name1"_"$date".zip 

  rm -rf $CURDIR/dti_parametric_maps_"$name1"_"$date"*

done

