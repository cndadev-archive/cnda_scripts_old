#!/bin/bash

#source /nrgpackages/scripts/xnat-tools_setup.sh



#get current date and set to variable mydate. used in log filenames
mydate=$(date +%Y_%m_%d_%M_%S)

echo "Running ADRC Clin Uploader"

#DIRECTORY LOCATIONS ----------------------------------------------------------------------------------------

#MAIN=/home/adrc/data/cnda/ADRC_EXPORT/CronScript
MAIN=$HOME/biostatTest


#Delete old XML directory if exists, create new one
rm -rf "${MAIN}/xml/adrc"
mkdir "${MAIN}/xml/adrc"

#Create log dir if it does not exist
if [ ! -d "${MAIN}/log" ]; then
  mkdir "${MAIN}/log"
fi


ADRC_LOG="${MAIN}/log/adrc_log_${mydate}.log"
ADRC_NOSEND="${MAIN}/log/adrc_noSend_${mydate}.log"
echo "No adrc XMLs created for these samples:" > ${ADRC_NOSEND}
ADRC_SEND="${MAIN}/log/adrc_XMLlog_${mydate}.log"
echo "ADRC XML docs created for these samples:" > ${ADRC_SEND}
ADRC_XML="${MAIN}/adrc.xml"
ADRC_PATH="${MAIN}/xml/adrc"
UTIL_SCRIPT="${MAIN}/getColumnForLabel.sh"
ADRC_SUBJ="${MAIN}/log/adrc_subjects.csv"
ADRC_UPLOAD="${MAIN}/log/adrc_upload_log_${mydate}.log"
echo "XML docs uploaded: " > ${ADRC_UPLOAD}
ADRC_LIVE="${MAIN}/log/adrc_Clins.csv"
MAIL="${MAIN}/daily.muttrc"
OUTPUT="${MAIN}/log/output_${mydate}.log"
CONFG="${MAIN}/config.txt"
oldGrant=not
oldSubj=not


# -----------------------------------------------------------------------------------------------------------   



#Send output(stdout) and errors (stderror) to the specified locations
#exec 1>"${OUTPUT}"
#exec 2>"${ERROR}"
exec >${OUTPUT} 2>&1

#Start usage function
# Usage output for explaining how to use the program
function usage
{
	echo "${SCRIPTNAME} does something so cool."
	echo ""
	echo "Usage: ${SCRIPTNAME}.sh (OPTIONS) [-H host] -K usernameFile -i inputDataFile -c configFile -o outputDataFile -w workingDir -p project -s subject -e experiment -t pattern -v verbose"	
	echo "   REQUIRED"
	echo "-H || --host            Host to connect to in format:  http(s)://yoursite.domain.ext"
	echo "-K || 				   File containing username and password"
	#echo "-U || --userid          User id to make connection."
	#userid now specified in config.txt
    echo "                        User id and host are optional if .xnatPass file is used."
	echo ""
	echo "   OPTIONAL"  
    #echo "-P || --password        Password can be provided on the command line, in the"
    #password now specified in config.txt
    echo "                        .xnatPass file or when prompted by the system."    
	echo "-i || --input           Path and filename from which to import data"
	echo "-c || --config          Path and filename from which to import configuration information."
    echo "-v || --verbose         Run script in verbose mode."
    echo ""
}
#end usage function

#Date parser start
# Expects one input parameter of date input of m/d/yyyy, returns yyyy-mm-dd
function reformatDate {
   dateStr="$1"

		day=`echo "$dateStr" | cut -d'/' -f"2"`
		month=`echo "$dateStr" | cut -d'/' -f"1"`
		year=`echo "$dateStr" | cut -d'/' -f"3"`
		if [ "${day}" -lt "10" ]; then
		day=0${day}
		fi
		if [ "${month}" -lt "10" ]; then
			month=0${month}
		fi
		echo ${year}"-"${month}"-"${day}

}
#date parser end

#Date parser -second start
#Using to return date in a different format
# Expects one input parameter of date input of yyyy-mm-dd, returns yyyymmdd
function reformatDate2 {
   dateStr="$1"

		month=`echo "$dateStr" | cut -d'-' -f"2"`
		year=`echo "$dateStr" | cut -d'-' -f"1"`
		day=`echo "$dateStr" | cut -d'-' -f"3"`
		#if [ "${day}" -lt "10" ]; then
		#	day=0${day}
		#fi
		#if [ "${month}" -lt "10" ]; then
		#	month=0${month}
		#fi
		echo ${year}${month}${day}

}
#date parser -second end

#Start getValueForColumnLabel Function -- label headers values
# Assumes well formed csv
function getValueForColumnLabel {
if [ $# -lt "3" ]
	then
		echo "Not enough arguments"
    else
        label=$1
		#if [ $verbose ]; then
		#	echo label $label
		#fi
		headers=$2
		#if [ $verbose ]; then
		#	echo headers $headers
		#fi
		values=$3
		#if [ $verbose ]; then
		#	echo values $values
		#fi
		colNum=`${UTIL_SCRIPT} -l "${label}" -s ${headers}`
		#if [ $verbose ]; then
		#	echo "getValueForColumnLabel colNum: " ${colNum}
		#fi
		
        tmpStr=""
        quoteCols=`echo ${values} | awk -F '\"' '{print NF}'`
                if [ "$quoteCols" -gt "1" ]; then
                    cleanStr=""
                    for (( c=1; c<="${quoteCols}"; c++ ))
                    do
                           isOdd=$((${c}%2))
                           if [ "$isOdd" = "1" ]; then
                                cleanStr=`echo $values | awk -F'\"' -v awkvar=$c '{print $awkvar}' | sed 's/,/!!;/g'`
                           else
                                cleanStr=`echo $values | awk -F'\"' -v awkvar=$c '{print $awkvar}'`
                           fi
                           tmpStr=${tmpStr}${cleanStr}
                    done
                    echo `echo ${tmpStr} | awk -F'!!;' -v awkvar=$colNum '{print $awkvar}'`
                else
                    echo `echo ${values} | awk -F',' -v awkvar=$colNum '{print $awkvar}'`
                fi
fi				
}

#End of getValueForColumnLabel function


# If no arguments are passed, display usage screen
if [ $# == 0 ]
then
	usage
	exit 1
fi

# Scan through user provided arguments.
while [ "$1" != "" ]; do
	case $1 in
		-H | --host )
			shift
            host=$1
			;;
		-U | --userid )
			shift
			userid=$1
			;;
		-P | --password )
			shift
			password=$1
			;;
		-i | --input )
			shift
			inputfile=$1
			;;
		-c | --config )
			shift
			configfile=$1
			;;
        -v | --verbose )
			verbose=1
			;;
		-h | --help )
			usage
			exit 1
    esac
    shift
done

# Host is required. If not, echo error message and usage output.
if [ -z "$host" ]; then
	echo "Host required."
	echo ""	
	usage
	exit 1
fi


# If input file supplied, make sure it exists.
if [ ! -z "$input" ]
then
	if [ ! -s "$inputfile" ]; then
            echo "The input file you specified either does not exist or is empty."
            echo ""
            usage
            exit 1			
	fi
fi


# ****************************************************************************
# Start of Updates -	October 2014	
	# Create a new jsession 
	JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""` 
	
	echo "JSession label: " ${JSESSION} >> ${ADRC_LOG}
	echo -e "\n================================================================" >> ${ADRC_LOG}
	
# End of Updates - October 2014
# ****************************************************************************






echo "Input file is "$inputfile >> ${ADRC_LOG}

# If verbose, then indicate that 
if [ $verbose ]; then
        echo "Running in verbose mode"
fi


let lineCount=`wc -l ${inputfile} | awk '{print $1}'` 

echo "The input file contains how many records (including header row)? "${lineCount}  >> ${ADRC_LOG}
echo -e "\n================================================================" >> ${ADRC_LOG}

if [ $verbose ]; then
        echo lineCount is ${lineCount} 
		
fi

                                                          
TMPFILE="${MAIN}/tmpfile.csv"  

# Get first line from input file
dataHeaders=`awk 'NR=='1'' "${inputfile}"`        
                                            

# Skip the reprepartion of the tempfile
if [ -z ${skipFilePrep} ]; then

  # Send Column headers to tmpfile, just remove the , and "
  echo "${dataHeaders}" > ${TMPFILE}

fi # end skipFilePrep

# Find number of lines in input file (minus header row)
let tmpfileCount=`cat ${inputfile} | wc -l`


# Get list of all subjects from host for project (ADRC), save (overwrite) to file in ADRC_SUBJ
#curl "${host}/data/archive/projects/ADRC/subjects?format=csv&columns=xnat:subjectData/ID,xnat:subjectData/label" > ${ADRC_SUBJ}
curl -b JSESSIONID=$JSESSION  -k "${host}/data/archive/projects/ADRC/subjects?format=csv&columns=xnat:subjectData/ID,xnat:subjectData/label" > ${ADRC_SUBJ}
	if [ $verbose ]; then
		echo "subject file created." 
    fi

# find subject label to correspond with subject id input at function call
function getSubjectLabel {
 
	if [ ! -z ${ADRC_SUBJ} ]; then
 
		#find line in file with corresponding subject label. -r is recursive, -w is whole word, -e is pattern match(exact)
		line=`cat ${ADRC_SUBJ} | grep -w -re "${1}"`
		
		#removing the quotes in the line
		lineWhole=`echo $line | tr -d '"'`
		
		#extracting subjectID -- for some reason had to to this twice for needed results.
		crop1=${lineWhole%*,*}
		subjectLabel=${crop1%*,*}

	else
		echo "No subjects found"
		exit 1
	fi

	#return subect label
	echo ${subjectLabel}
}

# Generate XML dir
if [ ! -d ./xml ]; then
	mkdir xml
fi 
cd xml


let duplicate=0

# Using project_id, subject_id, and the collection date, get the visit_id and the collection session id
# getValueForColumnLabel label headers values

#Collect values from row and assign to variables
let k=2

while [ "$k" -le "${tmpfileCount}" ]
do   
	currLine=`awk 'NR=='$k'' "${inputfile}"` 
	if [ $verbose ]; then
		echo currentLine $currLine
    fi
	
	subjectLabel=`getValueForColumnLabel "ID" "${dataHeaders}" "${currLine}"` 	
	echo "Subject: " ${subjectLabel} >> ${ADRC_LOG}	
	if [ $verbose ]; then
		echo subject $subjectLabel
    fi
	subjectID=`getSubjectLabel "${subjectLabel}"`
		echo "SubjectID: " ${subjectID} >> ${ADRC_LOG}	
	if [ $verbose ]; then
		echo subject $subjectID
    fi
	date=`getValueForColumnLabel "testdate" "${dataHeaders}" "${currLine}"` 
	if [ ! -z ${date} ]; then
		dateMod=`reformatDate2 "${date}"`
		#date=`reformatDate "${date}"`
	fi
	echo "Date: " ${date}>> ${ADRC_LOG}
	if [ $verbose ]; then
		echo date $date
    fi	
	visit_id="visit_${dateMod}"
	if [ $verbose ]; then
		echo visit_id ${visit_id}  
    fi
	
	
# ****************************************************************************
# Start of Updates - May 2013	

# Adding functionality to pull data from the HEIGHT, WEIGHT, and acsparnt columns in the input spreadsheet

	height=`getValueForColumnLabel "HEIGHT" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo HEIGHT ${height}  
    fi
	
	
 	weight=`getValueForColumnLabel "WEIGHT" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo WEIGHT ${weight}  
    fi
	
	
	acsparnt=`getValueForColumnLabel "acsparnt" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo acsparnt ${acsparnt}  
    fi
	
	
#End of Updates
# ******************************************************************************	


	
	
# ****************************************************************************
# Start of Updates -	October 2014, December 2014

# Adding functionality to pull data from the newly added GRANT column in the input spreadsheet

	grant=`getValueForColumnLabel "GRANT" "${dataHeaders}" "${currLine}"` 
	primStudy=`getValueForColumnLabel "Prim_Study" "${dataHeaders}" "${currLine}"`
	acsStudy=`getValueForColumnLabel "ACS_STUDY" "${dataHeaders}" "${currLine}"`
	
		#if [ $verbose ]; then
		echo GRANT ${grant}  >> ${ADRC_LOG}
		echo Prim_Study ${primStudy} >> ${ADRC_LOG}
		echo ACS_Study ${acsStudy}  >> ${ADRC_LOG}
    #fi
#End of Updates
# ******************************************************************************	

	
	memory=`getValueForColumnLabel "memory" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo memory ${memory}  
    fi

	orient=`getValueForColumnLabel "orient" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo orient ${orient}  
    fi

	judgment=`getValueForColumnLabel "judgment" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo judgment ${judgment}  
    fi

	commun=`getValueForColumnLabel "commun" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo commun ${commun}  
    fi

	homehob=`getValueForColumnLabel "homehobb" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo homehob ${homehob}  
    fi

	perscare=`getValueForColumnLabel "perscare" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo perscare ${perscare}  
    fi

	cdr=`getValueForColumnLabel "cdr" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo cdr ${cdr}  
    fi

	mmse=`getValueForColumnLabel "MMSE" "${dataHeaders}" "${currLine}"` 
	if [ $verbose ]; then
		echo mmse ${mmse}  
    fi

	sumbox=`getValueForColumnLabel "sumbox" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo sumbox ${sumbox}  
    fi

	dx1=`getValueForColumnLabel "dx1" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo dx1 ${dx1}  
    fi
	dx2=`getValueForColumnLabel "dx2" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo dx2 ${dx2}  
    fi
	dx3=`getValueForColumnLabel "dx3" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo dx3 ${dx3}  
    fi
	age=`getValueForColumnLabel "age_at_entry" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo age_at_entry ${age}  
    fi

	dx4=`getValueForColumnLabel "dx4" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo dx4 ${dx4}  
    fi
	dx5=`getValueForColumnLabel "dx5" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo dx5 ${dx5}  
    fi
	apoe=`getValueForColumnLabel "APOE" "${dataHeaders}" "${currLine}"`
	if [ $verbose ]; then
		echo apoe ${apoe}  
    fi
	

	
	label=${subjectLabel}_CLIN_${dateMod} 
	echo "Session label: "${label} >> ${ADRC_LOG}
	echo "-----------------------------------" >> ${ADRC_LOG}
			
			# Create the ADRC XML

			if [ -f ${ADRC_PATH}/${label}.xml ]; then
				echo ${proj} ${subjectLabel} ${visit_id} ${dateMod} ERROR: duplicate records >> ${ADRC_NOSEND}
				echo "******************Duplicate record found: "${proj} ${subjectLabel} ${visit_id} ${dateMod} >> ${ADRC_UPLOAD}
				let duplicate=$duplicate+1
			else 
				#Make a copy of the generic xml document then add values from the current line
				cp ${ADRC_XML} ${ADRC_PATH}/${label}.xml
				#the sed method can be used for columns that will never be empty
				#-------------------------------
				#commented out project b/c hardcoded in xml
				#sed -i 's/PROJECT/'${proj}'/' ${ADRC_PATH}/${label}.xml
				#visit id not needed in this xml
				#sed -i 's/label2/'${mydate}${k}'/' ${ADRC_PATH}/${label}.xml
				sed -i 's/label1/'${label}'/g' ${ADRC_PATH}/${label}.xml	
			    #----------------------------
				#Any columns that may have a null value must be handled as in if statement below
				#M and m are zero values and will not be added to the xml document
				if [ ! -z "${date}" ]; then
					echo \<xnat:date\>${date}\<\/xnat:date\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${subjectID}" ]; then
					if [ "${subjectID}" != "M" -a "${subjectID}" != "m" ]; then
						echo \<xnat:subject_ID\>${subjectID}\<\/xnat:subject_ID\> >> ${ADRC_PATH}/${label}.xml
					fi	
				fi
				if [ ! -z "${mmse}" ]; then
					if [ "${mmse}" != "M" -a "${mmse}" != "m" ]; then
						echo \<adrc:mmse\>${mmse}\<\/adrc:mmse\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				if [ ! -z "${age}" ]; then
					if [ "${age}" != "M" -a "${age}" != "m" ]; then
						echo \<adrc:ageAtEntry\>${age}\<\/adrc:ageAtEntry\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi

				if [ ! -z "${cdr}" ]; then
					if [ "${cdr}" != "M" -a "${cdr}" != "m" ]; then
						echo \<adrc:cdr\>${cdr}\<\/adrc:cdr\> >> ${ADRC_PATH}/${label}.xml
					fi
		
				fi
				if [ ! -z "${commun}" ]; then
					if [ "${commun}" != "M" -a "${commun}" != "m" ]; then
						echo \<adrc:commun\>${commun}\<\/adrc:commun\> >> ${ADRC_PATH}/${label}.xml
					fi
		
				fi
				if [ ! -z "${dx1}" ]; then
					echo \<adrc:dx1\>${dx1}\<\/adrc:dx1\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${dx2}" ]; then
					echo \<adrc:dx2\>${dx2}\<\/adrc:dx2\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${dx3}" ]; then
					echo \<adrc:dx3\>${dx3}\<\/adrc:dx3\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${dx4}" ]; then
					echo \<adrc:dx4\>${dx4}\<\/adrc:dx4\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${dx5}" ]; then
					echo \<adrc:dx5\>${dx5}\<\/adrc:dx5\> >> ${ADRC_PATH}/${label}.xml
				fi
				if [ ! -z "${homehob}" ]; then
					if [ "${homehob}" != "M" -a "${homehob}" != "m" ]; then
						echo \<adrc:homehobb\>${homehob}\<\/adrc:homehobb\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				if [ ! -z "${judgment}" ]; then
					if [ "${judgment}" != "M" -a "${judgment}" != "m" ]; then
						echo \<adrc:judgment\>${judgment}\<\/adrc:judgment\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
	
				if [ ! -z "${memory}" ]; then
					if [ "${memory}" != "M" -a "${memory}" != "m" ]; then
						echo \<adrc:memory\>${memory}\<\/adrc:memory\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				if [ ! -z "${orient}" ]; then
					if [ "${orient}" != "M" -a "${orient}" != "m" ]; then
						echo \<adrc:orient\>${orient}\<\/adrc:orient\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
			    if [ ! -z "${perscare}" ]; then
					if [ "${perscare}" != "M" -a "${perscare}" != "m" ]; then
						echo \<adrc:perscare\>${perscare}\<\/adrc:perscare\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				if [ ! -z "${apoe}" ]; then
					if [ "${apoe}" != "M" -a "${apoe}" != "m" ]; then
						echo \<adrc:apoe\>${apoe}\<\/adrc:apoe\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				if [ ! -z "${sumbox}" ]; then
					if [ "${sumbox}" != "M" -a "${sumbox}" != "m" ]; then
						echo \<adrc:sumbox\>${sumbox}\<\/adrc:sumbox\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				
# ****************************************************************************
# Start of Updates - May 2013

# Adding functionality to put height, weight, acsparnt values in the xml document (unless value is M or m)	

				if [ ! -z "${height}" ]; then
					if [ "${height}" != "M" -a "${height}" != "m" ]; then
						echo \<adrc:height\>${height}\<\/adrc:height\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				
				
				if [ ! -z "${weight}" ]; then
					if [ "${weight}" != "M" -a "${weight}" != "m" ]; then
						echo \<adrc:weight\>${weight}\<\/adrc:weight\> >> ${ADRC_PATH}/${label}.xml
					fi
				fi
				
				
				if [ ! -z "${acsparnt}" ]; then
					if [ "${acsparnt}" != "M" -a "${acsparnt}" != "m" ]; then
						if [ "${acsparnt}" = "0" ]; then
							acsparnt="false"
							echo \<adrc:acsparnt\>${acsparnt}\<\/adrc:acsparnt\> >> ${ADRC_PATH}/${label}.xml
						else
							acsparnt="true"
							echo \<adrc:acsparnt\>${acsparnt}\<\/adrc:acsparnt\> >> ${ADRC_PATH}/${label}.xml
						fi
					fi
				fi
				
# End of Updates
# *************************************************************************************	



# *************************************************************************************
# Start of Updates - December 2014
# Adding functionality to put primStudy and acsStudy values in the xml document (unless value is M or m)

	if [ ! -z "${primStudy}" ]; then
		if [ "${primStudy}" != "M" -a "${primStudy}" != "m" ]; then
			echo \<adrc:primStudy\>"${primStudy}"\<\/adrc:primStudy\> >> ${ADRC_PATH}/${label}.xml
		fi
		
	fi
	
	
	if [ ! -z "${acsStudy}" ]; then
		if [ "${acsStudy}" != "M" -a "${acsStudy}" != "m" ]; then
			echo \<adrc:acsStudy\>"${acsStudy}"\<\/adrc:acsStudy\> >> ${ADRC_PATH}/${label}.xml
		fi
		
	fi



# End of Updates
# *************************************************************************************	




# ****************************************************************************
# Start of Updates - Oct 2014

# Adding functionality to put grant value in a Custom Variable associated with the Subject

# Sample query: curl -X PUT -k -b JSESSIONID=$JSESSION "${host}/data/archive/projects/ADRC/subjects/10034?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=NEWTEST"
#               Accessing custom variable labeled "Grant" (must be in lowercase in query) and assigning value "NEWTEST"
#  variable to send is: ${grant} 
#  curl -X PUT -k -b JSESSIONID=$JSESSION "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=${grant}"

if [ -n $grant -a "$grant" != " " ]; then

	#check if grant data already sent
	if [ ${subjectLabel} != ${oldSubj} -o ${grant} != ${oldGrant} ]; then
		
		# Send data
		#echo "Before Send : JSESSIONID= " $JSESSION "host: " ${host} "subject label: " ${subjectLabel} "grant: " ${grant}  >> ${ADRC_LOG}
		echo "time before Grant send: " $(date) >> ${ADRC_LOG}
		echo "curl -X PUT -b JSESSIONID=$JSESSION  -k ${host}/data/archive/projects/ADRC/subjects/${subjectLabel}?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=${grant}" >> ${ADRC_LOG}
		sent=`curl -X PUT -b JSESSIONID=$JSESSION  -k "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=${grant}"`
		echo "Send Results for Grant: " ${sent} >> ${ADRC_LOG}
		echo "time after Grant send: " $(date) >> ${ADRC_LOG}
		
		# Check if Curl send failed -- failure produces html code
		failedGrant=`echo "${sent}" | head -1 | grep html`
			if [ ! -z "${failedGrant}" ]; then		
				# Create new jsession, try curl again
				JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""` 	
				echo "NEW JSession label: " ${JSESSION} >> ${ADRC_LOG}
				echo -e "\n================================================================" >> ${ADRC_LOG}
				echo "curl -X PUT -b JSESSIONID=$JSESSION  -k ${host}/data/archive/projects/ADRC/subjects/${subjectLabel}?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=${grant}" >> ${ADRC_LOG}
				
				sent3=`curl -X PUT -b JSESSIONID=$JSESSION  -k "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}?xsiType=xnat:subjectData&xnat:subjectData/fields/field%5Bname%3Dgrant%5D/field=${grant}"`
				
				echo "Send Results, 2nd Attempt, for Grant: " ${sent3} >> ${ADRC_LOG}
						totalFail=`echo "${sent3}" | head -1 | grep html`
						if [ ! -z "${totalFail}" ]; then	
							# On 2nd fail to send Grant info, make a log entry and continue
							echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> ${ADRC_NOSEND}
							echo "The following Grant information failed to upload for Subject: " ${subjectLabel} "," ${grant} >> ${ADRC_NOSEND}
							echo "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" >> ${ADRC_NOSEND}
						fi
			fi

	fi
		oldGrant=${grant}
		oldSubj=${subjectLabel}
		
fi


# End of Updates
# *************************************************************************************					


				
				
				echo "</adrc:ADRCClinical>"  >> ${ADRC_PATH}/${label}.xml
				# Update the send log
				echo "Session label: " ${label} >> ${ADRC_SEND}
				
				#echo "curl : " $JSESSION  ${ADRC_PATH}"/"${label} "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}/experiments/${label}.xml" >> ${ADRC_LOG}
				echo "time before xml send: " $(date) >> ${ADRC_LOG}
				
				
				#Upload xml here for ease of use of curl statement -- had trouble with this until the addition of "?inbody=true"
				send2=`curl -b JSESSIONID=$JSESSION -X PUT -T ${ADRC_PATH}/${label}.xml -k "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}/experiments/${label}.xml?inbody=true"`
				echo "Send Results for XML: " ${send2} >> ${ADRC_LOG}
				echo "time after xml send: " $(date) >> ${ADRC_LOG}
				echo "-----------------------------------" >> ${ADRC_LOG}
				
				echo "==============${label}===============" >> ${ADRC_UPLOAD}
				sleep 2
				
				
				# Check if Curl send failed -- failure produces html code
				failedXML=`echo "${send2}" | head -1 | grep html`
					if [ ! -z "${failedXML}" ]; then		
						# Create new jsession, try curl again
						JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""` 	
						echo "NEW JSession label: " ${JSESSION} >> ${ADRC_LOG}
						echo -e "\n================================================================" >> ${ADRC_LOG}
						send3=`curl -b JSESSIONID=$JSESSION -X PUT -T ${ADRC_PATH}/${label}.xml -k "${host}/data/archive/projects/ADRC/subjects/${subjectLabel}/experiments/${label}.xml?inbody=true"`
						
						echo "Send Results, 2nd attempt, for XML: " ${send3} >> ${ADRC_LOG}
						
						totalFail2=`echo "${send3}" | head -1 | grep html`
						if [ ! -z "${totalFail2}" ]; then
							# On 2nd fail to send xml, make a log entry and continue
							echo "The following xml failed to upload: " ${label} >> ${ADRC_NOSEND}
						fi
					fi				
				
				

			fi
	

			
	let k=$k+1	
done
# End ADRC loop


# Create new jsession
JSESSION=`curl -K "${CONFG}" -k ""${host}/REST/JSESSION""`


# If there were any errors, don't send successfully completed to log file
isErr=`cat ${ADRC_NOSEND} | grep "ERROR"`
                                                                  
if [ -z "${isErr}" ]; then
	 echo "Successfully completed file: " ${inputfile} >> ${ADRC_LOG}
fi



# Confirm the number of records now in CNDA matches number sent
# ##lineCount is number of records in input spreadsheet

# Query ADRC project, return a list of all ADRCClins, save as csv to ${ADRC_LIVE}
curl -b JSESSIONID=$JSESSION  -k "${host}/data/archive/projects/ADRC/subjects?format=csv&columns=adrc:ADRCClinicalData/ID" > ${ADRC_LIVE}



## Get first line from csv file
dataHeadersLive=`awk 'NR=='1'' "${ADRC_LIVE}"` 

## Find number of lines in live file (minus header row)
let liveFileCount=`cat ${ADRC_LIVE} | wc -l`


let clinCount=0

# loop through LIVE file
let q=2
while [ "$q" -le "${liveFileCount}" ]
do  
    currLineLive=`awk 'NR=='$q'' "${ADRC_LIVE}"` 

	clinID=`getValueForColumnLabel "adrc:adrcclinicaldata/id" "${dataHeadersLive}" "${currLineLive}"` 
	if [ ! -z ${clinID} ]; then
		let clinCount=$clinCount+1
	fi
let q=$q+1	
done
# end loop through LIVE file



# Calculate number or records: Number of rows in input file, minus duplicates, minus header row
let actualLineCount=${lineCount}-${duplicate}-1

# Diff between number of records and number of experiments in project (live)
let diff=${actualLineCount}-${clinCount}

echo "################## Final Counts #####################" >> ${ADRC_UPLOAD}
echo "Records in input file: " ${actualLineCount} >> ${ADRC_UPLOAD}
echo "Live files count: " ${clinCount} >> ${ADRC_UPLOAD}

echo "Difference between files input and files live (ignoring duplicates): "  ${diff} >> ${ADRC_UPLOAD}




# Email upload log to CNDA
#modified to send all log files as attachment to email and embed main result in message body
mailResult=`echo -e "ADRC Clin log files are attached. \n Results: Difference between ADRC Clin files uploaded and files in CNDA:  ${diff}" | /usr/bin/mutt -s "ADRC Upload Log for ${mydate}" -F ${MAIL} CNDA-Ops@nrg.wustl.edu -a ${ADRC_LOG} -a ${ADRC_NOSEND}`

echo "Result of email send: " ${mailResult} >> ${ADRC_UPLOAD}


# Delete temp file
rm ${TMPFILE}

# ****************************************************************************
# Start of Updates - Oct 2014

	# close jsession
	# sample: curl -b "JSESSIONID=B43AB8A989E3A026E5E539DF09A01B0B" -X DELETE https://central.xnat.org/data/JSESSION
	curl -X DELETE -b JSESSIONID=$JSESSION -k "${host}/REST/JSESSION"

# End of Updates
# *************************************************************************************	
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   