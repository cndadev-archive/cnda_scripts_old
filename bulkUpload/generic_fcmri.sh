#!/bin/bash

# Script for bulk upload for Jim Alexopoulos
# Written by Jenny Gurney

# Directory where data is found
FILEDIR=/data/nil-bluearc/shimony/wunder
# Directory where files are copied temporarily and zip files created
TMPDIR=/data/nil-bluearc/marcus/gkgurney/jimA
# Directory for QC logs
QCLOGDIR=/data/nil-bluearc/marcus/gkgurney/qc-logs
# Configuration file--format 4 columns:  <old file/dir name> <subject id> <new file/dir name> <session id>
CFGFILE=wu_caffeine_fcmri_uploads_all_20101018.txt
# Project id
PROJECT=wu_caffeine
# User id
USERID=gkgurney
# Password
PASSWD=temp4Now
# XNAT host (CNDA url)
XNATHOST=https://cnda.wustl.edu
# Path to CNDA REST client software
RESTPATH=/data/cninds01/data2/nrg-tools/XNATRestClient

# If configuration file not found, output message, exit script
if [ ! -f $CFGFILE ]; then
  echo Configuration file was not found: $CFGFILE
  exit 0;
fi

# Number of lines in configuration file (ie., number of zip files to be uploaded)
lineCount=`wc -l $CFGFILE | awk '{print $1}'`
echo Configuration file line count: $lineCount

# If there are no lines in the configuration file, notify, then exit
if [ "$lineCount" -lt "1" ]; then
  echo Configuration file $CFGFILE is empty
  exit 0;
fi

# Gets today's date
date=`date "+%Y%m%d"`
echo Current date: "$date"

# If qc log directory doesn't exist, then create it
if [ ! -d $QCLOGDIR ]; then
  mkdir $QCLOGDIR
  echo Created directory:  $QCLOGDIR
fi

# Create output file
outputFile=fcmri_qc_$date.log
touch "$QCLOGDIR"/"$outputFile"

# Initializes loop counter            
let i=0

# Loop through each line in configuration file 1-lineCount
while [ "$i" -lt "$lineCount" ]
do
  # increment line number 
  let i=$i+1
  echo Processing line number: $i

  # get old directory name (column 1)
  name1=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $1}'`
  echo name1: $name1
  # get subject (column 2)
  name2=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $2}'`
  echo name2: $name2
  # get new file/directory name (column 3)
  name3=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $3}'`
  echo name3: $name3
  # get experiment (column 4)
  name4=`awk 'NR=='$i'' "$CFGFILE"  | awk '{print $4}'`
  echo name4: $name4

  # Make temporary directory to copy files
  mkdir $TMPDIR/fcmri_"$name3"_$date

  # Copy files into temporary directory just created
  /bin/cp -r -p $FILEDIR/$name1/*blur $TMPDIR/fcmri_"$name3"_"$date"
  /bin/cp -r -p $FILEDIR/$name1/FCmaps $TMPDIR/fcmri_"$name3"_"$date"
  /bin/cp -r -p $FILEDIR/$name1/regressors $TMPDIR/fcmri_"$name3"_"$date"
  /bin/cp -r -p $FILEDIR/$name1/*blur_yz $TMPDIR/fcmri_"$name3"_"$date"

  # Delete backup directories
  
  execdir=`pwd`
  cd $TMPDIR/fcmri_"$name3"_"$date"
  find . -name "*backup*" -exec rm -rf {} \ 
  cd $execdir

  # Zip up all the files in the temporary directory
  cd $TMPDIR
  zip -r $TMPDIR/fcmri_"$name3"_"$date".zip fcmri_"$name3"_"$date" 
  cd $execdir

  # Ouput zip file details to QC log file
  ls -lh $TMPDIR/fcmri_"$name3"_"$date".zip >> $QCLOGDIR/$outputFile

  # Upload to CNDA session using REST client
  $RESTPATH -host $XNATHOST -u $USERID -p $PASSWD -m PUT -remote /REST/projects/$PROJECT/subjects/"$name2"/experiments/"$name4"/resources/fcmri/files/fcmri_"$name3"_"$date".zip -local "$TMPDIR"/fcmri_"$name3"_"$date".zip 

  # Confirm that file was uploaded and output information to QC log file
  $RESTPATH -host $XNATHOST -u $USERID -p $PASSWD -m GET -remote /REST/projects/$PROJECT/subjects/"$name2"/experiments/"$name4"/resources/fcmri >> $QCLOGDIR/$outputFile

  # Remove temporary directory and zip file 
  rm -rf $TMPDIR/fcmri_"$name3"_"$date"*

done

