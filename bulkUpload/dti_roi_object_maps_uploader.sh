#!/bin/bash

# Script to do bulk upload of processed files to wu_premie and wu_caffeine
 
WRKDIR=`pwd`
DESCSTR=dti_roi_object_maps
PROGRAM=dti_roi_object_maps_uploader.sh
USAGE="echo -e Usage: $PROGRAM -u <user id> -p <project> -c <config input file> -f <files for upload>"
XNATHOST=https://cnda.wustl.edu
LOGDIR=$WRKDIR/logs-$DESCSTR
LSLOG=$DESCSTR-filesize.log
RESTLOG=$DESCSTR-resttest.log

date=`date "+%Y%m%d"`
#echo date: $date

TMPDIR=$WRKDIR/.temp_CNDA_$DESCSTR_$date
rm -rf $TMPDIR
mkdir $TMPDIR


if [ ! -d $LOGDIR ]; then
  mkdir $LOGDIR 
  echo Creating log directory: $LOGDIR
fi

while [ $# -gt 0 ]
do
  if [ $1 == "-u" ]; then
    shift
    USERID=$1
    echo CNDA user id: $1
    shift 
  elif [ $1 == "-p" ]; then
    shift
    PROJECT=$1
    echo project: $1
    shift
  elif [ $1 == "-c" ]; then
    shift
    CFGFILE=$1
    echo config file: $1
    shift
  elif [ $1 == "-f" ]; then
    shift
    CURDIR=$1
    echo config file: $1
    shift
fi 
done

# If user id not entered, then exit 
if [ -z $USERID ]; then
  echo No user id entered 
  #echo $USAGE 
  $USAGE
  exit 0
fi

# If project not entered, then exit     
#if [ -z $PROJECT ]; then
#  echo No project entered 
#  echo $USAGE
#  exit 0
#fi
 
# If config file not specified, then exit 
if [ -z $CFGFILE ]; then
  echo No config file entered
  echo $USAGE
  exit 0
fi  

# If upload file directory not specified, then exit 
if [ -z $CURDIR ]; then
  echo Source directory location of upload files not specified
  echo $USAGE
  exit 0
fi 

# If config file does not exist or is empty, then exit
if [ ! -s $CFGFILE ]; then
  echo The configuration file you specified either does not exist or is empty.
  exit 0
fi

# get session id
JSESSION=`curl -k -u $USERID ""https://cnda.wustl.edu/REST/JSESSION""` 
responseCount=`echo $JSESSION | wc -m`
if [ $responseCount -gt 33 ]; then
  echo CNDA log in failed. Please check your password and retry.
  exit 0
fi
     

lineCount=`wc -l $CFGFILE | awk '{print $1}'`
# echo line count: $lineCount
            
let i=0
while [ "$i" -lt "$lineCount" ]

do
  let i=$i+1

  #echo `awk 'NR=='$i'' "$CFGFILE"`
  #get file/directory name
  project=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $1}'`
  # echo project: $project
  #get subject
  subject=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $2}'`
  # echo subject: $subject
  #get experiment
  experiment=`awk 'NR=='$i'' "$CFGFILE"  | sed -e 's/,/ /g;s/"//g' | awk '{print $3}'`
  # echo experiment: $experiment
  #get filename
  filename=`awk 'NR=='$i'' "$CFGFILE" | sed -e 's/,/ /g;s/"//g;s///g' | awk '{print $4}'`
  # echo filename: $filename 
    
  echo Uploading "$filename" to "$project"/"$subject"/"$experiment"/"$DESCSTR" \($i of $lineCount\)

  outputLine=`ls -l "$CURDIR"/"$filename"`
  echo `date` $outputLine >> $LOGDIR/$LSLOG   
  curl -k -b JSESSIONID=$JSESSION -T $CURDIR/$filename -X POST ""https://cnda.wustl.edu/REST/projects/$project/subjects/"$subject"/experiments/"$experiment"/resources/"$DESCSTR"/files/$filename?inbody=true"" 
  
  curl -k -b JSESSIONID=$JSESSION ""https://cnda.wustl.edu/REST/projects/$project/subjects/"$subject"/experiments/"$experiment"/resources/"$DESCSTR"/files?format=csv"" > $TMPDIR/restouput.tmp    
  outputLine=`awk 'NR==2' "$TMPDIR/restouput.tmp"`
  echo `date` $outputLine >> $LOGDIR/$RESTLOG
 
done

rm -rf $TMPDIR
